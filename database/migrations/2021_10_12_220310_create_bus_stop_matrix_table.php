<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBusStopMatrixTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bus_stop_matrix', function (Blueprint $table) {
            $table->foreignUuid('origin_id', 36)->references('id')->on('bus_stops')->comment('Origin bus stop');//->primary();
            $table->foreignUuid('destination_id', 36)->references('id')->on('bus_stops')->comment('Destination bus stop');//->primary();
            $table->integer('origin_prodater_id');
            $table->integer('destination_prodater_id');
            $table->integer('distance');
            $table->integer('duration');
            $table->timestamps();

            $table->primary(['origin_id','destination_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bus_stop_matrix');
    }
}
