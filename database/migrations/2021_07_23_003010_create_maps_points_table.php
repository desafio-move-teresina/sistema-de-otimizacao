<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMapsPointsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('maps_points', function (Blueprint $table) {
            
            $table->uuid('map', 36)->comment('Map identifier');
            $table->uuid('point', 36)->comment('Point identifier');

            $table->foreign('map')->references('id')->on('maps');
            $table->foreign('point')->references('id')->on('points');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('maps_points');
    }
}
