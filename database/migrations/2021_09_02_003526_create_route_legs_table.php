<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRouteLegsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('route_legs', function (Blueprint $table) {
            $table->uuid('id', 36)->primary()->comment('Leg primary key');
            $table->unsignedInteger('distance')->comment('Leg\'s distance');
            $table->unsignedInteger('duration')->comment('Leg\'s duration');
            
            $table->uuid('routeId', 36)->comment('Route FK');
            $table->timestamps();

            $table->foreign('routeId')->references('id')->on('routes');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('route_legs');
    }
}
