<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRouteBusStopsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('route_bus_stops', function (Blueprint $table) {
            $table->uuid('routeId', 36)->comment('Route identifier');
            $table->uuid('busStopId', 36)->comment('Bus stop identifier');
            $table->primary(['routeId', 'busStopId']);

            $table->foreign('routeId')->references('id')->on('routes');
            $table->foreign('busStopId')->references('id')->on('bus_stops');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('route_bus_stops');
    }
}
