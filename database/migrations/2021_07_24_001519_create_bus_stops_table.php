<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBusStopsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bus_stops', function (Blueprint $table) {
            $table->uuid('id', 36)->comment('Point identifier')->primary();
            $table->double('latitude', 16, 13);
            $table->double('longitude', 16, 13);
            $table->double('radius');
            $table->double('direction');
            $table->boolean('isActive');
            $table->integer('passengersIn');
            $table->integer('passengersOut');
            $table->integer('prodater_code');
            $table->string('prodater_name')->nullable();
            $table->string('prodater_address')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bus_stops');
    }
}
