<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVehiclesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vehicles', function (Blueprint $table) {
            $table->uuid('id', 36)->primary()->comment('Vehicle primary key');
            $table->char('licencePlate', 8)->comment('Vehicle licence plate');
            $table->unsignedSmallInteger('manufactureYear')->comment('Vehicle year of manufacture');
            $table->string('prefix')->comment('Vehicle prefix');
            $table->unsignedSmallInteger('seatedCapacity')->comment('Vehicle capacity of seated passengers');
            $table->unsignedSmallInteger('totalCapacity')->comment('Total passenger capacity for the vehicle');
            $table->string('status', 40)->comment('Vehicle status');
            $table->string('vehicleType', 100)->comment('Vehicle type');
            $table->boolean('hasElevator')->comment('If vehicle has an elevator for wheelchair');
            
            $table->uuid('companyId', 36)->comment('Company FK');
            $table->timestamps();

            $table->foreign('companyId')->references('id')->on('companies');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vehicles');
    }
}
