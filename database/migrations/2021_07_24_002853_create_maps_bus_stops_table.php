<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMapsBusStopsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('maps_bus_stops', function (Blueprint $table) {
            $table->uuid('map', 36)->comment('Map identifier');
            $table->uuid('bus_stop', 36)->comment('Point identifier');
            $table->primary(['map', 'bus_stop']);

            $table->foreign('map')->references('id')->on('maps');
            $table->foreign('bus_stop')->references('id')->on('bus_stops');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('maps_bus_stops');
    }
}
