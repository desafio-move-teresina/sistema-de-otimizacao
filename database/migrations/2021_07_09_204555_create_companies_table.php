<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCompaniesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('companies', function (Blueprint $table) {
            $table->uuid('id', 36)->comment('company identifier')->primary();
            $table->string('name', 50)->comment('Company Name');
            $table->string('cnpj', 20)->comment('Company cnpj');
            $table->string('consortium', 50)->comment('Company Consortium');
            $table->string('phone', 50)->comment( 'Company phone');
            $table->string('email', 50)->comment( 'Company email');
            $table->string('address', 255)->comment( 'Company address');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('companies');
    }
}
