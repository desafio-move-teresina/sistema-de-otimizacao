<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRoutesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('routes', function (Blueprint $table) {
            $table->uuid('id', 36)->comment('Route identifier')->primary();
            $table->string('name', 50)->comment('Route Name');
            $table->unsignedSmallInteger('estimatedPassengers')->comment('average # of passengers in this route');
            $table->unsignedInteger('estimatedDistance')->comment('average distance of this route');
            $table->unsignedInteger('estimatedDuration')->comment('average duration of this route');
            $table->text('overviewPolyline')->comment('The complete polyline for the route');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('routes');
    }
}
