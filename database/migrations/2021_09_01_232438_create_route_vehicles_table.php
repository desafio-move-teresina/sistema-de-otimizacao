<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRouteVehiclesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('route_vehicles', function (Blueprint $table) {
            $table->uuid('id', 36)->primary()->comment('Vehicle primary key');
            $table->unsignedSmallInteger('totalCapacity')->comment('Total passenger capacity for the vehicle');
            
            $table->uuid('routeId', 36)->comment('Route FK');
            $table->timestamps();

            $table->foreign('routeId')->references('id')->on('routes');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('route_vehicles');
    }
}
