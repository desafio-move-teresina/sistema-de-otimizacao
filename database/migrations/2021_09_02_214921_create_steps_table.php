<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStepsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('steps', function (Blueprint $table) {
            $table->uuid('id', 36)->primary()->comment('Step primary key');
            $table->unsignedInteger('distance')->comment('step\'s distance');
            $table->unsignedInteger('duration')->comment('step\'s duration');
            $table->text('htmlInstruction')->comment('Step information formatted in HTML');
            $table->text('polyline')->comment('Step Polyline');
            $table->string('maneuver')->comment('Direction to follow')->nullable();
            
            $table->uuid('routeLegId', 36)->comment('Route leg FK');
            $table->timestamps();

            $table->foreign('routeLegId')->references('id')->on('route_legs');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('steps');
    }
}
