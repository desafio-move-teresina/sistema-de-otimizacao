<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePlacesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('places', function (Blueprint $table) {
            $table->uuid('id', 36)->comment('Point identifier')->primary();
            $table->uuid('busStop', 36)->comment('Point identifier');
            $table->string('name', 50)->comment('Name Place');
            $table->timestamps();

            $table->foreign('busStop')->references('id')->on('bus_stops');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('places');
    }
}
