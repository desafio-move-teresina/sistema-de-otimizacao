<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBasicRulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('basic_rules', function (Blueprint $table) {

            $table->uuid('id', 36)->comment('configuration code')->primary();
            $table->double('stopMinimumDistance')->comment('the bus must avoid stopping when going through adistance less than this (in meters)');
            $table->double('stopMaximumDistance')->comment('the bus must avoid traveling a distance greater than this without stopping (in meters)');
            $table->double('maximumWait')->comment('the passenger should not wait any longer than this time(in minutes)');
            $table->double('defaultBusStopRadius')->comment('default value of the radius at which the bus stop serves the population (in meters)');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('basic_rules');
    }
}
