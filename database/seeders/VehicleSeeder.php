<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Str;
use App\Models\api\ResourceCompany\Company;
use App\Models\api\ResourceVehicles\Vehicle;

class VehicleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $numberOfVehicles = 150;
        $i = 0;

        do {
            Vehicle::create(
                [
                    "id" => "",
                    "licencePlate" => strtoupper(Str::random(3))."-".sprintf("%04d", rand(0,9999)),
                    "manufactureYear" => mt_rand(1995, 2021),
                    "prefix" => mt_rand(1995, 2021),
                    "seatedCapacity" => mt_rand(5, 8)*5,
                    "totalCapacity" => mt_rand(8, 12)*5,
                    "status" => ["active", "sold", "maintenance"][mt_rand(0,2)],
                    "vehicleType" => ['micro', 'mini', 'mid', 'basic', 'standard', 'articulated', 'bi-articulated'][mt_rand(0,6)],
                    "hasElevator" => mt_rand(0, 1),
                    "companyId" => Company::inRandomOrder()->first()->id,
                ],
            );
        } while ($i++ < $numberOfVehicles);
    }
}
