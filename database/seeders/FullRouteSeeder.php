<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use \App\Http\Traits\RouteTrait;
use App\Models\api\ResourceVehicles\Vehicle;
use App\Models\api\ResourceMap\BusStop;

class FullRouteSeeder extends Seeder
{
    use RouteTrait;
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $routeQuantity = 2;
        $jsonData = "[";

        do{
            $estimatedPassengers = mt_rand(50, 200);
            $estimatedDistance = mt_rand(10000, 20000);
            $estimatedDuration = mt_rand(3000, 7000);

            $jsonData .= 
<<<EOT
{
    "routeId": $routeQuantity,
    "vehicles" : [
EOT;
            foreach(Vehicle::get()->random(3) as $vehicle) {
                $jsonData .= '{"id" : "'.$vehicle->id.'"},';
            }
            $jsonData = rtrim($jsonData, ",");

            $jsonData .= 
<<<EOT
    ],
    "busStop": [
EOT;

            foreach(BusStop::get()->random(6) as $busStop) {
                $jsonData .= '{"id" : "'.$busStop->id.'"},';
            }
            $jsonData = rtrim($jsonData, ",");

            $jsonData .= 
<<<EOT
    ],
    "estimatedPassengers": $estimatedPassengers,
    "estimatedDistance": $estimatedDistance,
    "estimatedDuration": $estimatedDuration
},
EOT;
        } while ($routeQuantity-- > 1);
        $jsonData = rtrim($jsonData, ",")."]";
        
        $fp = fopen('seederJsonData.json', 'w');
        $data = fwrite($fp, $jsonData);
        fclose($fp);

        $routes = json_decode($jsonData, true);

        foreach($routes as $route) {
            $this->createNewRoute($route);
        }
    }
}