<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\api\ResourceMap\BusStop;

class BusStopSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $client = new \GuzzleHttp\Client();
        $busStops = [];
        $page = 0;

        do {
            $res = $client->request(
                'GET',
                env('PRODATER_BASE_URL')."/paradas?size=3000&page=$page",
                [
                    'auth' => [
                        env('PRODATER_USER'),
                        env('PRODATER_PASSWORD')
                    ]
                ]
            );
            $busStops = array_merge($busStops, json_decode($res->getBody())->content);
            $page++;
        } while (json_decode($res->getBody())->totalPages > json_decode($res->getBody())->pageable->pageNumber+1);

        foreach($busStops as $origin) {
            BusStop::create([
                "id" => "",
                "latitude" => $origin->coordenadaY,
                "longitude" => $origin->coordenadaX,
                "radius" => 600,
                "direction" => 30,
                "isActive" => ($origin->situacaoParada == 'A'),
                "passengersIn" => 8,
                "passengersOut" => 5,
                "prodater_code" => $origin->codigoParada,
                "prodater_name" => $origin->nomeParada,
                "prodater_address" => $origin->endereco
            ]);
        }
    }
}
