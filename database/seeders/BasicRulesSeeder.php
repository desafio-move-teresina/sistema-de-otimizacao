<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\api\ResourceConfiguration\BasicRule;

class BasicRulesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        BasicRule::create(
            [
                "id" => "",
                "stopMinimumDistance" => 300,
                "stopMaximumDistance" => 500,
                "maximumWait" => 45,
                "defaultBusStopRadius" => 600,
            ]);
    }
}
