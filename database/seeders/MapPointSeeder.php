<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\api\ResourceMap\Point;
use App\Models\api\ResourceMap\Map;

class MapPointSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $map = Map::first();
        
        Point::all()->each(function ($point) use ($map) { 
            $point->maps()->attach($map); 
        });
    }
}
