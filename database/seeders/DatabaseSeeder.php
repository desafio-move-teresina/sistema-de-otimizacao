<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $a = new \App\Http\Controllers\api\ResourceMap\MapEditorController();
        // dd($a->index());
        $this->call([
            BasicRulesSeeder::class,
            CompanySeeder::class,
            VehicleSeeder::class,
            MapSeeder::class,
            BusStopSeeder::class,
            MapBusStopSeeder::class,
            PointSeeder::class,
            MapPointSeeder::class,
            // FullRouteSeeder::class,
            // BusStopMatrixSeeder::class,
        ]);
    }
}
