<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use \App\Http\Controllers\api\ResourceBusStop\BusStopController;

class BusStopMatrixSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // can be used to create the distance matrix for the bus stops from Prodater
        $createMatrix = new BusStopController();
        $createMatrix->generateBusStopMatrix();
    }
}
