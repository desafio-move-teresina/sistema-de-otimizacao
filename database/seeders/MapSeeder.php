<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\api\ResourceMap\Map;

class MapSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Map::create(
            [
                "id" => "",
                "title" => "Mapa Seeder 1",
                "area" => 25485265,
                // "json_map" => ""
            ]);
    }
}
