<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\api\ResourceCompany\Company;

class CompanySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Consórcio Poty
        Company::create(
            [
                "id" => "",
                "name" => "VIAÇÃO PIAUIENSE LTDA",
                "cnpj" => "11.803.322/0001-95",
                "consortium" => "Consórcio Poty",
                "phone" => "(86) 2156-5425",
                "email" => "email@teresina.com",
                "address" => "Rua em Teresina 1",
            ]);
        Company::create(
            [
                "id" => "",
                "name" => "EMPRESA VIAÇÃO PIAUI LTDA",
                "cnpj" => "44.391.717/0001-87",
                "consortium" => "Consórcio Poty",
                "phone" => "(86) 2156-5425",
                "email" => "email@teresina.com",
                "address" => "Rua em Teresina 1",
            ]);
        Company::create(
            [
                "id" => "",
                "name" => "TRANSPORTES COLETIVOS CIDADE VERDE LTDA",
                "cnpj" => "87.398.591/0001-85",
                "consortium" => "Consórcio Poty",
                "phone" => "(86) 2156-5425",
                "email" => "email@teresina.com",
                "address" => "Rua em Teresina 1",
            // Consórcio Urbanus
            ]);
        Company::create(
            [
            "id" => "",
                "name" => "TRANSPORTES COLETIVOS CIDADE VERDE LTDA",
                "cnpj" => "60.241.936/0001-36",
                "consortium" => "Consórcio Urbanus",
                "phone" => "(86) 2156-5425",
                "email" => "email@teresina.com",
                "address" => "Rua em Teresina 1",
            ]);
        Company::create(
            [
                "id" => "",
                "name" => "VIAÇÃO SANTANA LTDA",
                "cnpj" => "40.333.377/0001-87",
                "consortium" => "Consórcio Urbanus",
                "phone" => "(86) 2156-5425",
                "email" => "email@teresina.com",
                "address" => "Rua em Teresina 1",
            ]);
        Company::create(
            [
                    "id" => "",
                "name" => "TRANSPORTES SÃO CRISTOVÃO LTDA",
                "cnpj" => "10.160.579/0001-03",
                "consortium" => "Consórcio Urbanus",
                "phone" => "(86) 2156-5425",
                "email" => "email@teresina.com",
                "address" => "Rua em Teresina 1",
            // Consórcio Teresina
            ]);
        Company::create(
            [
                "id" => "",
                "name" => "EMTRACOL – EMPRESA DE TRANSPORTES COLETIVOS LTDA",
                "cnpj" => "59.281.011/0001-30",
                "consortium" => "Consórcio Teresina",
                "phone" => "(86) 2156-5425",
                "email" => "email@teresina.com",
                "address" => "Rua em Teresina 1",
            ]);
        Company::create(
            [
                    "id" => "",
                "name" => "TAGUATUR TAGUATINGA TRANSPORTE E TURISMO LTDA",
                "cnpj" => "17.198.471/0001-59",
                "consortium" => "Consórcio Teresina",
                "phone" => "(86) 2156-5425",
                "email" => "email@teresina.com",
                "address" => "Rua em Teresina 1",
            ]);
        Company::create(
            [
                    "id" => "",
                "name" => "TRANSPORTES THEREZINA LTDA",
                "cnpj" => "44.711.920/0001-93",
                "consortium" => "Consórcio Teresina",
                "phone" => "(86) 2156-5425",
                "email" => "email@teresina.com",
                "address" => "Rua em Teresina 1",
            ]);
        Company::create(
            [
                "id" => "",
                "name" => "EXPRESSO SANTA CRUZ LTDA",
                "cnpj" => "61.262.431/0001-10",
                "consortium" => "Consórcio Teresina",
                "phone" => "(86) 2156-5425",
                "email" => "email@teresina.com",
                "address" => "Rua em Teresina 1",
            ]);
        Company::create(
            [
                "id" => "",
                "name" => "TRASNFÁCIL TRASNPORTE COLETIVO LTDA",
                "cnpj" => "05.261.676/0001-52",
                "consortium" => "Consórcio Teresina",
                "phone" => "(86) 2156-5425",
                "email" => "email@teresina.com",
                "address" => "Rua em Teresina 1",
            // TRANSCOL
            ]);
        Company::create(
            [
                "id" => "",
                "name" => "TRANSCOL – TRANSPORTES COLETIVOS LTDA",
                "cnpj" => "59.642.010/0001-73",
                "consortium" => "TRANSCOL",
                "phone" => "(86) 2156-5425",
                "email" => "email@teresina.com",
                "address" => "Rua em Teresina 1",
            ],
        );
    }
}
