<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\api\ResourceMap\Point;

class PointSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Point::create([
            "latitude" => -5.079615,
            "longitude" => -42.822028,
            "order" => 1,
        ]);
        Point::create([
            "latitude" => -5.070814,
            "longitude" => -42.804571,
            "order" => 2,
        ]);
        Point::create([
            "latitude" => -5.094421,
            "longitude" => -42.781399,
            "order" => 3,
        ]);
        Point::create([
            "latitude" => -5.100149,
            "longitude" => -42.811355,
            "order" => 4,
        ]);
    }
}
