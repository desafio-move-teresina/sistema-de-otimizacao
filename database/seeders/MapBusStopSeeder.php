<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\api\ResourceMap\BusStop;
use App\Models\api\ResourceMap\Map;

class MapBusStopSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $map = Map::first();
        
        BusStop::all()->each(function ($busStop) use ($map) { 
            $busStop->maps()->attach($map); 
        });
    }
}
