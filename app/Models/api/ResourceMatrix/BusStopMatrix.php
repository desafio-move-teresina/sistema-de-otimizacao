<?php

namespace App\Models\api\ResourceMatrix;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
use App\Models\api\ResourceMap\BusStop;
use GoldSpecDigital\LaravelEloquentUUID\Database\Eloquent\Uuid;
use Illuminate\Notifications\Notifiable;

/**
 * @OA\Schema(
 *      title="BusStopMatrix",
 *      description="Keeps the data returned from google on distance and duration on the route",
 *      type="object"
 * )
 */
class BusStopMatrix extends Model
{
    use Uuid;
    use HasFactory,Notifiable;

    protected $table = 'bus_stop_matrix';

    /**
     * @OA\Property(
     *      property="id",
     *      title="id",
     *      description="Bus Stop identifier",
     *      example="02031ea8-eac4-4757-b3c4-de8f66d4b357"
     * )
     *
     * @var String
     */
    protected $primaryKey = 'origin_id';//['origin_id', 'destination_id']; -> declaração removida pois dá erro "array to string conversion na hora de inserir o registro
    public $incrementing = false;

    protected $fillable = [
        'origin_id',
        'destination_id',
        /**
        * @OA\Property(
        *      property="distance",
        *      title="distance",
        *      description="Distance from the bus stops",
        *      example="600"
        * )
        *
        * @var Integer
        */
        'distance',
        /**
        * @OA\Property(
        *      property="duration",
        *      title="duration",
        *      description="Duration from one bus stop to the other",
        *      example="600"
        * )
        *
        * @var Integer
        */
        'duration',
        /**
        * @OA\Property(
        *      property="origin_prodater_id",
        *      title="origin_prodater_id",
        *      description="Prodater code for origin bus stop",
        *      example=600
        * )
        *
        * @var Integer
        */
        'origin_prodater_id',
        /**
        * @OA\Property(
        *      property="destination_prodater_id",
        *      title="destination_prodater_id",
        *      description="Prodater code for destination bus stop",
        *      example=600
        * )
        *
        * @var Integer
        */
        'destination_prodater_id',
        /**
         * @OA\Property(
         *      property="created_at",
         *      title="created_at",
         *      description="created in (Date)",
         *      example="2021-07-08 11:48:11"
         * )
         *
         * @var string
         */
        'created_at',
        /**
         * @OA\Property(
         *      property="updated_at",
         *      title="updated_at",
         *      description="updated (Date)",
         *      example="2021-07-08 11:48:11"
         * )
         *
         * @var string
         */
        'updated_at',
    ];

    public function originBusStop() {
        return $this->belongsTo(BusStop::class, 'origin_id' , 'id');
    } 

    public function destinationBusStop() {
        return $this->belongsTo(BusStop::class, 'destination_id' , 'id');
    } 

}