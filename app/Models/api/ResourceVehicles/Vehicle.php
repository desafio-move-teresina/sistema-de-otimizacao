<?php

namespace App\Models\api\ResourceVehicles;

use App\Models\api\ResourceCompany\Company;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
use Illuminate\Notifications\Notifiable;
use GoldSpecDigital\LaravelEloquentUUID\Database\Eloquent\Uuid;

/**
 * @OA\Schema(
 *      title="Vehicle",
 *      description="Show Vehicle response body data",
 *      type="object"
 * )
 */
class Vehicle extends Model
{
    use Uuid;
    use HasFactory,Notifiable;

    protected $table = 'vehicles';
    /**
     * @OA\Property(
     *      property="id",
     *      title="id",
     *      description="Vehicle id",
     *      example="02031ea8-eac4-4757-b3c4-de8f66d4b357"
     * )
     *
     * @var String
     */
    protected $primaryKey = 'id';

    protected $fillable = [
        /**
        * @OA\Property(
        *      property="licencePlate",
        *      title="Licence Plate",
        *      description="Vehicle licence plate",
        *      example="PXL-5885"
        * )
        *
        * @var String
        */
        'licencePlate',
        /**
        * @OA\Property(
        *      property="manufactureYear",
        *      title="Manufacture Year",
        *      description="Year the vehicle was manufactu",
        *      example=2021
        * )
        *
        * @var String
        */
        'manufactureYear',
        /**
        * @OA\Property(
        *      property="prefix",
        *      title="Prefix",
        *      description="Vehicle prefix code",
        *      example="A582"
        * )
        *
        * @var String
        */
        'prefix',
        /**
        * @OA\Property(
        *      property="seatedCapacity",
        *      title="Seated Capacity",
        *      description="Maximum of seated passagers",
        *      example="50"
        * )
        *
        * @var Integer
        */
        'seatedCapacity',
        /**
        * @OA\Property(
        *      property="totalCapacity",
        *      title="Total Capacity",
        *      description="Maximum of passagers",
        *      example="120"
        * )
        *
        * @var Integer
        */
        'totalCapacity',
        /**
        * @OA\Property(
        *      property="status",
        *      title="Status",
        *      description="Status of the vehicle",
        *      example="Active"
        * )
        *
        * @var String
        */
        'status',
        /**
        * @OA\Property(
        *      property="vehicleType",
        *      title="Vehicle Type",
        *      description="Type of vehicle",
        *      example="Microonibus"
        * )
        *
        * @var String
        */
        'vehicleType',
        /**
        * @OA\Property(
        *      property="hasElevator",
        *      title="Has Elevator",
        *      description="Indicates if the vehicle has handicappted elevator",
        *      example="true"
        * )
        *
        * @var Boolean
        */
        'hasElevator',
        /**
        * @OA\Property(
        *      property="companyId",
        *      title="Company",
        *      description="Id of the company related to the vehicle",
        *      example="4f9495ab-9ba0-4a40-971a-936dfb11d21f"
        * )
        *
        * @var String
        */
        'companyId',
        /**
         * @OA\Property(
         *      property="created_at",
         *      title="created_at",
         *      description="created in (Date)",
         *      example="2021-07-08 11:48:11"
         * )
         *
         * @var string
         */
        'created_at',
        /**
         * @OA\Property(
         *      property="updated_at",
         *      title="updated_at",
         *      description="updated (Date)",
         *      example="2021-07-08 11:48:11"
         * )
         *
         * @var string
         */
        'updated_at',
    ];

     //===============Geração GUID ===========================
     protected $guarded = [];
     protected static function boot()
     {
         parent::boot();
 
         static::creating(function ($post) {
             $post->{$post->getKeyName()} = (string) Str::uuid();
         });
     }
     public function getIncrementing()
     {
         return false;
     }
 
     public function getKeyType()
     {
         return 'string';
     }

    //=============== Relacionamento de tabela ===========================
    public function company(){
        return $this->belongsTo(Company::class, 'companyId', 'id');
    }
}
