<?php

namespace App\Models\api\ResourceMap;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
use GoldSpecDigital\LaravelEloquentUUID\Database\Eloquent\Uuid;
use Illuminate\Notifications\Notifiable;

/**
 * @OA\Schema(
 *      title="BusStop",
 *      description="Show bus stop response body data",
 *      type="object"
 * )
 */
class BusStop extends Model
{
    use Uuid;
    use HasFactory,Notifiable;

    protected $table = 'bus_stops';

    /**
     * @OA\Property(
     *      property="id",
     *      title="id",
     *      description="Bus Stop identifier",
     *      example="02031ea8-eac4-4757-b3c4-de8f66d4b357"
     * )
     *
     * @var String
     */
    protected $primaryKey = 'id';

    protected $fillable = [
        /**
        * @OA\Property(
        *      property="radius",
        *      title="radius",
        *      description="Scope radius",
        *      example="600"
        * )
        *
        * @var Integer
        */
        'radius',
        /**
        * @OA\Property(
        *      property="direction",
        *      title="direction",
        *      description="Direction in degrees",
        *      example="90"
        * )
        *
        * @var Integer
        */
        'direction',
        /**
        * @OA\Property(
        *      property="longitude",
        *      title="longitude",
        *      description="Longitude of the bus stop",
        *      example="-47.465498"
        * )
        *
        * @var Decimal
        */
        'longitude',
        /**
        * @OA\Property(
        *      property="latitude",
        *      title="latitude",
        *      description="Latitude of the bus stop",
        *      example="-4.465498"
        * )
        *
        * @var Decimal
        */
        'latitude',
        /**
        * @OA\Property(
        *      property="isActive",
        *      title="isActive",
        *      description="Whether the bus stop is active",
        *      example="1"
        * )
        *
        * @var Boolean
        */
        'isActive',
        /**
        * @OA\Property(
        *      property="fixed",
        *      title="fixed",
        *      description="Indicate that this station should be used in the coverage problem",
        *      example="1"
        * )
        *
        * @var Boolean
        */
        'fixed',
        /**
        * @OA\Property(
        *      property="passengersIn",
        *      title="passengersIn",
        *      description="How many passengers get in the bus",
        *      example="4"
        * )
        *
        * @var Integer
        */
        'passengersIn',
        /**
        * @OA\Property(
        *      property="passengersOut",
        *      title="passengersOut",
        *      description="How many passengers get off the bus",
        *      example="4"
        * )
        *
        * @var Integer
        */
        'passengersOut',
        /**
        * @OA\Property(
        *      property="prodaterCode",
        *      title="prodaterCode",
        *      description="Id returned from Prodater",
        *      example=4
        * )
        *
        * @var Integer
        */
        'prodater_code',
        /**
        * @OA\Property(
        *      property="prodaterName",
        *      title="prodaterName",
        *      description="Name returned from Prodater",
        *      example="Terminal Alto da Ressurreição"
        * )
        *
        * @var Integer
        */
        'prodater_name',
        /**
        * @OA\Property(
        *      property="prodaterAddress",
        *      title="prodaterAddress",
        *      description="Address returned from Prodater",
        *      example="Rua Luís Abreu, 683-703 - Gurupi, Teresina - PI, Brasil"
        * )
        *
        * @var Integer
        */
        'prodater_address',
        /**
        * @OA\Property(
        *      property="centralStation",
        *      title="centralStation",
        *      description="Indicates if this bus stop is considered a central station",
        *      example="1"
        * )
        *
        * @var Boolean
        */
        'central_station',
        /**
         * @OA\Property(
         *      property="created_at",
         *      title="created_at",
         *      description="created in (Date)",
         *      example="2021-07-08 11:48:11"
         * )
         *
         * @var string
         */
        'created_at',
        /**
         * @OA\Property(
         *      property="updated_at",
         *      title="updated_at",
         *      description="updated (Date)",
         *      example="2021-07-08 11:48:11"
         * )
         *
         * @var string
         */
        'updated_at',
    ];

    //===============Geração GUID ===========================
    protected $guarded = [];
    protected static function boot()
    {
        parent::boot();

        static::creating(function ($post) {
            $post->{$post->getKeyName()} = (string) Str::uuid();
        });
    }
    public function getIncrementing()
    {
        return false;
    }

    public function getKeyType()
    {
        return 'string';
    }

    //===============Many to many ===========================
    public function maps() {
        return $this->belongsToMany(Map::class, 'maps_bus_stops' , 'bus_stop', 'map');
    } 

    //===============One to many===========================
    public function places() {
        return $this->hasMany(Place::class, 'busStop' , 'id');
    }

    public function setLatitudeAttribute($value)
    {
        $this->attributes["latitude"] = number_format($value, 13);
    }
    public function setLongitudeAttribute($value)
    {
        $this->attributes["longitude"] = number_format($value, 13);
    }
}
