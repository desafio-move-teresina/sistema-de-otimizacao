<?php

namespace App\Models\api\ResourceMap;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
use GoldSpecDigital\LaravelEloquentUUID\Database\Eloquent\Uuid;
use Illuminate\Notifications\Notifiable;

/**
 * @OA\Schema(
 *      title="Place",
 *      description="Show Place response body data",
 *      type="object"
 * )
 */
class Place extends Model
{
    use Uuid;
    use HasFactory,Notifiable;

    protected $table = 'places';

    /**
     * @OA\Property(
     *      property="id",
     *      title="id",
     *      description="Place identifier",
     *      example="02031ea8-eac4-4757-b3c4-de8f66d4b357"
     * )
     *
     * @var String
     */
    protected $primaryKey = 'id';

    protected $fillable = [
        /**
         * @OA\Property(
         *      property="busStop",
         *      title="busStop",
         *      description="Bus stop related to this place",
         *      example="02031ea8-eac4-4757-b3c4-de8f66d4b357"
         * )
         *
         * @var String
         */
        'busStop',
        /**
         * @OA\Property(
         *      property="name",
         *      title="name",
         *      description="Place name",
         *      example="Escola Central"
         * )
         *
         * @var String
         */
        'name',
        /**
         * @OA\Property(
         *      property="created_at",
         *      title="created_at",
         *      description="created in (Date)",
         *      example="2021-07-08 11:48:11"
         * )
         *
         * @var string
         */
        'created_at',
        /**
         * @OA\Property(
         *      property="updated_at",
         *      title="updated_at",
         *      description="updated (Date)",
         *      example="2021-07-08 11:48:11"
         * )
         *
         * @var string
         */
        'updated_at',
    ];

    //===============Geração GUID ===========================
    protected $guarded = [];
    protected static function boot()
    {
        parent::boot();

        static::creating(function ($post) {
            $post->{$post->getKeyName()} = (string) Str::uuid();
        });
    }
    public function getIncrementing()
    {
        return false;
    }

    public function getKeyType()
    {
        return 'string';
    }
}
