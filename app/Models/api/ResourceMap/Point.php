<?php

namespace App\Models\api\ResourceMap;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
use GoldSpecDigital\LaravelEloquentUUID\Database\Eloquent\Uuid;
use Illuminate\Notifications\Notifiable;

/**
 * @OA\Schema(
 *      title="Point",
 *      description="Show the point information response body data",
 *      type="object"
 * )
 */
class Point extends Model
{
    use Uuid;
    use HasFactory,Notifiable;

    protected $table = 'points';

    /**
     * @OA\Property(
     *      property="id",
     *      title="id",
     *      description="Point identifier",
     *      example="02031ea8-eac4-4757-b3c4-de8f66d4b357"
     * )
     *
     * @var String
     */
    protected $primaryKey = 'id';

    protected $fillable = [
        /**
        * @OA\Property(
        *      property="longitude",
        *      title="longitude",
        *      description="Longitude of the point",
        *      example="-47.465498"
        * )
        *
        * @var Decimal
        */
        'longitude',
        /**
        * @OA\Property(
        *      property="latitude",
        *      title="latitude",
        *      description="Latitude of the point",
        *      example="-4.465498"
        * )
        *
        * @var Decimal
        */
        'latitude',
        /**
        * @OA\Property(
        *      property="order",
        *      title="order",
        *      description="To order the positions for the map",
        *      example=1
        * )
        *
        * @var Integer
        */
        'order',
        /**
         * @OA\Property(
         *      property="created_at",
         *      title="created_at",
         *      description="created in (Date)",
         *      example="2021-07-08 11:48:11"
         * )
         *
         * @var string
         */
        'created_at',
        /**
         * @OA\Property(
         *      property="updated_at",
         *      title="updated_at",
         *      description="updated (Date)",
         *      example="2021-07-08 11:48:11"
         * )
         *
         * @var string
         */
        'updated_at',
    ];

    //===============Geração GUID ===========================
    protected $guarded = [];
    protected static function boot()
    {
        parent::boot();

        static::creating(function ($post) {
            $post->{$post->getKeyName()} = (string) Str::uuid();
        });
    }
    public function getIncrementing()
    {
        return false;
    }

    public function getKeyType()
    {
        return 'string';
    }

    //===============Many to many ===========================
    public function maps(){
        return $this->belongsToMany(Map::class, 'maps_points' , 'point', 'map');
    } 
}
