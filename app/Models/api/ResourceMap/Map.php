<?php

namespace App\Models\api\ResourceMap;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
use GoldSpecDigital\LaravelEloquentUUID\Database\Eloquent\Uuid;
use Illuminate\Notifications\Notifiable;

/**
 * @OA\Schema(
 *      title="Map",
 *      description="Show Map response body data",
 *      type="object"
 * )
 */
class Map extends Model
{
    use Uuid;
    use HasFactory,Notifiable;

    protected $table = 'maps';

    /**
     * @OA\Property(
     *      property="id",
     *      title="id",
     *      description="Map identifier",
     *      example="02031ea8-eac4-4757-b3c4-de8f66d4b357"
     * )
     *
     * @var String
     */
    protected $primaryKey = 'id';

    protected $json_map = 'json_map';

    protected $fillable = [
        /**
        * @OA\Property(
        *      property="title",
        *      title="title",
        *      description="Map name",
        *      example="Map 54849"
        * )
        *
        * @var String
        */
        'title',
        /**
        * @OA\Property(
        *      property="area",
        *      title="area",
        *      description="Total area of the polygon",
        *      example=54855689
        * )
        *
        * @var Integer
        */
        'area',
        /**
         * @OA\Property(
         *      property="created_at",
         *      title="created_at",
         *      description="created in (Date)",
         *      example="2021-07-08 11:48:11"
         * )
         *
         * @var string
         */
        'created_at',
        /**
         * @OA\Property(
         *      property="updated_at",
         *      title="updated_at",
         *      description="updated (Date)",
         *      example="2021-07-08 11:48:11"
         * )
         *
         * @var string
         */
        'updated_at',
    ];

    //===============Geração GUID ===========================
    protected $guarded = [];
    protected static function boot()
    {
        parent::boot();

        static::creating(function ($post) {
            $post->{$post->getKeyName()} = (string) Str::uuid();
        });
    }
    public function getIncrementing()
    {
        return false;
    }

    public function getKeyType()
    {
        return 'string';
    }

    //===============Many to many ===========================
    public function points() {
        return $this->belongsToMany(Point::class, 'maps_points' , 'map', 'point')->orderBy('order', 'asc');
    } 
    
    //===============Many to many ===========================
    public function busStops() {
        return $this->belongsToMany(BusStop::class, 'maps_bus_stops' , 'map', 'bus_stop');
    } 
}
