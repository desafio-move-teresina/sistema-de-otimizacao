<?php

namespace App\Models\api\ResourceConfiguration;

use GoldSpecDigital\LaravelEloquentUUID\Database\Eloquent\Uuid;
use Illuminate\Support\Str;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @OA\Schema(
 *      title="Basic Rule",
 *      description="Show Basic Rule response body data",
 *      type="object"
 * )
 */
class BasicRule extends Model
{
    use Uuid;
    use HasFactory,Notifiable;

    protected $table = 'basic_rules';

    /**
     * @OA\Property(
     *      property="id",
     *      title="id",
     *      description="Basic rule identifier",
     *      example="02031ea8-eac4-4757-b3c4-de8f66d4b357"
     * )
     *
     * @var number
     */
    protected $primaryKey = 'id';


    protected $fillable = [
        /**
         * @OA\Property(
         *      property="stopMinimumDistance",
         *      title="stopMinimumDistance",
         *      description="the bus must avoid stopping when going through adistance less than this (in meters)",
         *      example="300"
         * )
         *
         * @var number
         */
        'stopMinimumDistance',
        /**
         * @OA\Property(
         *      property="stopMaximumDistance",
         *      title="stopMaximumDistance",
         *      description="the bus must avoid traveling a distance greater than this without stopping (in meters)",
         *      example="500"
         * )
         *
         * @var number
         */
        'stopMaximumDistance',
            /**
             * @OA\Property(
             *      property="maximumWait",
             *      title="maximumWait",
             *      description="the passenger should not wait any longer than this time(in minutes)",
             *      example="300"
             * )
             *
             * @var number
             */
        'maximumWait',
        /**
         * @OA\Property(
         *      property="defaultBusStopRadius",
         *      title="defaultBusStopRadius",
         *      description="default value of the radius at which the bus stop serves the population (in meters)",
         *      example="30"
         * )
         *
         * @var number
        */
        'defaultBusStopRadius',
        /**
         * @OA\Property(
         *      property="created_at",
         *      title="created_at",
         *      description="created in (Date)",
         *      example="2021-07-08 11:48:11"
         * )
         *
         * @var string
         */
        'created_at',
        /**
         * @OA\Property(
         *      property="updated_at",
         *      title="updated_at",
         *      description="updated (Date)",
         *      example="2021-07-08 11:48:11"
         * )
         *
         * @var string
         */
        'updated_at',
    ];

    //===============Geração GUID ===========================
    protected $guarded = [];
    protected static function boot()
    {
        parent::boot();

        static::creating(function ($post) {
            $post->{$post->getKeyName()} = (string) Str::uuid();
        });
    }
    public function getIncrementing()
    {
        return false;
    }

    public function getKeyType()
    {
        return 'string';
    }
}