<?php

namespace App\Models\api\ResourceCompany;

use App\Models\api\ResourceVehicles\Vehicle;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
use Illuminate\Notifications\Notifiable;
use GoldSpecDigital\LaravelEloquentUUID\Database\Eloquent\Uuid;

/**
 * @OA\Schema(
 *      title="Company",
 *      description="Show Company response body data",
 *      type="object"
 * )
 */
class Company extends Model
{
    use Uuid;
    use HasFactory,Notifiable;

    protected $table = 'companies';
    /**
     * @OA\Property(
     *      property="id",
     *      title="id",
     *      description="Company identifier",
     *      example="02031ea8-eac4-4757-b3c4-de8f66d4b357"
     * )
     *
     * @var String
     */
    protected $primaryKey = 'id';

    protected $fillable = [
        /**
        * @OA\Property(
        *      property="name",
        *      title="name",
        *      description="Company name",
        *      example="Optime"
        * )
        *
        * @var String
        */
        'name',
        /**
        * @OA\Property(
        *      property="cnpj",
        *      title="cnpj",
        *      description="Company cnpj",
        *      example="77.344.421/0001-64"
        * )
        *
        * @var String
        */
        'cnpj',
        /**
         * @OA\Property(
         *      property="consortium",
         *      title="consortium",
         *      description="Company consortium",
         *      example="Rotas"
         * )
         *
         * @var String
         */
        'consortium',
        /**
         * @OA\Property(
         *      property="phone",
         *      title="phone",
         *      description="Company phone",
         *      example="+5534998880101"
         * )
         *
         * @var String
         */
        'phone',
                    /**
         * @OA\Property(
         *      property="email",
         *      title="email",
         *      description="Company email",
         *      example="email@email.com"
         * )
         *
         * @var String
         */
        'email',
        /**
         * @OA\Property(
         *      property="address",
         *      title="address",
         *      description="Company address",
         *      example="Rua Joaquim José, Número 69"
         * )
         *
         * @var String
         */
        'address',
        /**
         * @OA\Property(
         *      property="created_at",
         *      title="created_at",
         *      description="created in (Date)",
         *      example="2021-07-08 11:48:11"
         * )
         *
         * @var String
         */
        'created_at',
        /**
         * @OA\Property(
         *      property="updated_at",
         *      title="updated_at",
         *      description="updated (Date)",
         *      example="2021-07-08 11:48:11"
         * )
         *
         * @var String
         */
        'updated_at',
    ];

     //===============Geração GUID ===========================
     protected $guarded = [];
     protected static function boot()
     {
         parent::boot();
 
         static::creating(function ($post) {
             $post->{$post->getKeyName()} = (string) Str::uuid();
         });
     }
     public function getIncrementing()
     {
         return false;
     }
 
     public function getKeyType()
     {
         return 'string';
     }

     public function vehicles(){
        return $this->hasMany(Vehicle::class, 'companyId' , 'id' );
    }
}
