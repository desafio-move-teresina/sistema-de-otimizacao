<?php

namespace App\Models\api\ResourceRoute;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
use GoldSpecDigital\LaravelEloquentUUID\Database\Eloquent\Uuid;
use Illuminate\Notifications\Notifiable;
use App\Models\api\ResourceMap\BusStop;
use App\Models\api\ResourceRoute\Optimization;

/**
 * @OA\Schema(
 *      title="Route",
 *      description="Show route response body data",
 *      type="object"
 * )
 */
class Route extends Model
{
    use Uuid, HasFactory,Notifiable;

    protected $table = 'routes';

    /**
     * @OA\Property(
     *      property="id",
     *      title="id",
     *      description="Route identifier",
     *      example="02031ea8-eac4-4757-b3c4-de8f66d4b357"
     * )
     *
     * @var String
     */
    protected $primaryKey = 'id';

    protected $fillable = [
        /**
        * @OA\Property(
        *      property="name",
        *      title="name",
        *      description="Company name",
        *      example="Optime"
        * )
        *
        * @var String
        */
        'name',
        /**
        * @OA\Property(
        *      property="estimatedPassengers",
        *      title="estimatedPassengers",
        *      description="Estimated number of passengers on the route",
        *      example=200
        * )
        *
        * @var Integer
        */
        'estimatedPassengers',
        /**
        * @OA\Property(
        *      property="estimatedDistance",
        *      title="estimatedDistance",
        *      description="Estimated distance run by this route, in meters",
        *      example=20000
        * )
        *
        * @var Integer
        */
        'estimatedDistance',
        /**
        * @OA\Property(
        *      property="estimatedDuration",
        *      title="estimatedDistance",
        *      description="Estimated duration in seconds",
        *      example=20000
        * )
        *
        * @var Integer
        */
        'estimatedDuration',
        /**
        * @OA\Property(
        *      property="overviewPolyline",
        *      title="overviewPolyline",
        *      description="Polyline returned by Google",
        *      example="LKFDS&$#JF*&kijsjwf*&Dsgdksdgs8sf"
        * )
        *
        * @var String
        */
        'overviewPolyline',
        /**
        * @OA\Property(
        *      property="optimizationId",
        *      title="optimizationId",
        *      description="Optimization group code",
        *      example="02031ea8-eac4-4757-b3c4-de8f66d4b357"
        * )
        *
        * @var String
        */
        'optimizationId',
        /**
         * @OA\Property(
         *      property="created_at",
         *      title="created_at",
         *      description="created in (Date)",
         *      example="2021-07-08 11:48:11"
         * )
         *
         * @var String
         */
        'created_at',
        /**
         * @OA\Property(
         *      property="updated_at",
         *      title="updated_at",
         *      description="updated (Date)",
         *      example="2021-07-08 11:48:11"
         * )
         *
         * @var String
         */
        'updated_at',
    ];

    //===============Geração GUID ===========================
    protected $guarded = [];
    protected static function boot()
    {
        parent::boot();

        static::creating(function ($post) {
            $post->{$post->getKeyName()} = (string) Str::uuid();
        });
    }

    public function getIncrementing()
    {
        return false;
    }

    public function getKeyType()
    {
        return 'string';
    }

    //=============== Many to many ===========================
    public function busStops() { 
        return $this->belongsToMany(BusStop::class, 'route_bus_stops' , 'routeId', 'busStopId');
    }

    public function routeVehicles() { 
        return $this->hasMany(RouteVehicle::class, 'routeId', 'id');
    }

    public function routeLegs() { 
        return $this->hasMany(RouteLeg::class, 'routeId', 'id');
    }

    public function optimization() {
        return $this->belongsTo(Optimization::class);
    } 

}