<?php

namespace App\Models\api\ResourceRoute;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
use GoldSpecDigital\LaravelEloquentUUID\Database\Eloquent\Uuid;
use Illuminate\Notifications\Notifiable;
use App\Models\api\ResourceRoute\Route;
use App\Models\api\ResourceRoute\Step;

/**
 * @OA\Schema(
 *      title="Route Leg",
 *      description="Show the legs for the route",
 *      type="object"
 * )
 */
class RouteLeg extends Model
{
    use Uuid, HasFactory,Notifiable;

    protected $table = 'route_legs';

    /**
     * @OA\Property(
     *      property="id",
     *      title="id",
     *      description="Route leg identifier",
     *      example="02031ea8-eac4-4757-b3c4-de8f66d4b357"
     * )
     *
     * @var String
     */
    protected $primaryKey = 'id';

    protected $fillable = [
        /**
        * @OA\Property(
        *      property="distance",
        *      title="distance",
        *      description="Distance run by this route leg, in meters",
        *      example=20000
        * )
        *
        * @var Integer
        */
        'distance',
        /**
        * @OA\Property(
        *      property="duration",
        *      title="duration",
        *      description="Duration in seconds",
        *      example=20000
        * )
        *
        * @var Integer
        */
        'duration',
        /**
        * @OA\Property(
        *      property="routeId",
        *      title="routeId",
        *      description="Route identifier",
        *      example="02031ea8-eac4-4757-b3c4-de8f66d4b357"
        * )
        *
        * @var String
        */
        'routeId',
        /**
         * @OA\Property(
         *      property="created_at",
         *      title="created_at",
         *      description="created in (Date)",
         *      example="2021-07-08 11:48:11"
         * )
         *
         * @var String
         */
        'created_at',
        /**
         * @OA\Property(
         *      property="updated_at",
         *      title="updated_at",
         *      description="updated (Date)",
         *      example="2021-07-08 11:48:11"
         * )
         *
         * @var String
         */
        'updated_at',
    ];


    //===============Geração GUID ===========================
    protected $guarded = [];
    protected static function boot()
    {
        parent::boot();

        static::creating(function ($post) {
            $post->{$post->getKeyName()} = (string) Str::uuid();
        });
    }

    public function getIncrementing()
    {
        return false;
    }

    public function getKeyType()
    {
        return 'string';
    }

    //=============== Many to many ===========================
    public function route() {
        return $this->belongsTo(Route::class);
    }

    public function steps() {
        return $this->hasMany(Step::class, 'routeLegId', 'id');
    } 
}