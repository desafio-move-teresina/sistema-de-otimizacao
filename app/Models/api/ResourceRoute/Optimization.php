<?php

namespace App\Models\api\ResourceRoute;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
use GoldSpecDigital\LaravelEloquentUUID\Database\Eloquent\Uuid;
use Illuminate\Notifications\Notifiable;

/**
 * @OA\Schema(
 *      title="Optimization",
 *      description="Group the routes in a optimization process",
 *      type="object"
 * )
 */
class Optimization extends Model
{
    use Uuid, HasFactory,Notifiable;

    protected $table = 'optimizations';

    /**
     * @OA\Property(
     *      property="id",
     *      title="id",
     *      description="Optimization identifier",
     *      example="02031ea8-eac4-4757-b3c4-de8f66d4b357"
     * )
     *
     * @var String
     */
    protected $primaryKey = 'id';

    protected $fillable = [
        /**
        * @OA\Property(
        *      property="name",
        *      title="name",
        *      description="Name of the optimization",
        *      example="20210101120000"
        * )
        *
        * @var String
        */
        'name',
        /**
         * @OA\Property(
         *      property="created_at",
         *      title="created_at",
         *      description="created in (Date)",
         *      example="2021-07-08 11:48:11"
         * )
         *
         * @var String
         */
        'created_at',
        /**
         * @OA\Property(
         *      property="updated_at",
         *      title="updated_at",
         *      description="updated (Date)",
         *      example="2021-07-08 11:48:11"
         * )
         *
         * @var String
         */
        'updated_at',
    ];

    //===============Geração GUID ===========================
    protected $guarded = [];
    protected static function boot()
    {
        parent::boot();

        static::creating(function ($post) {
            $post->{$post->getKeyName()} = (string) Str::uuid();
        });
    }

    public function getIncrementing()
    {
        return false;
    }

    public function getKeyType()
    {
        return 'string';
    }

    public function routes() {
        return $this->hasMany(Route::class, 'optimizationId', 'id');
    }
}