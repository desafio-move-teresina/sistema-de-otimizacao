<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Facades\DB;
use OpenApi\Annotations\Get;

class UniqueModel implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */

    private $columm;
    private $table;
    public function __construct($value, $value2)
    {
        $this->columm = $value;
        $this->table = $value2;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @var $columm 
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $result = (DB::table($this->table)->select('*')->where( $this->columm, '=', $value)->get());
  
        return !count($result)>0 ; 
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return ' already registered ';
    }
}
