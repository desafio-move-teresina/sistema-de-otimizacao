<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Facades\DB;

class ThereIsEntity implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    private $database; 
    private $colunm;
    public function __construct($value1, $value2)
    {
        $this->database = $value1;
        $this->colunm = $value2;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $result = (DB::table($this->database)->where($this->colunm, '=' , $value)->get());

        return count($result)>0 ; 
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Entity '.$this->database.' does not exist.';
    }
}
