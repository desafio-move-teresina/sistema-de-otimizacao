<?php

namespace App\Http\Requests\api\ResourceCompany;

use App\Rules\ModelIntegrity;
use Illuminate\Foundation\Http\FormRequest;

/**
 * @OA\Schema(
 *      title="Update Company Request",
 *      description="Show Company Request",
 *      type="object"
 * )
 */
class UpdateCompanyRequest extends FormRequest
{
      /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [

            /**
            * @OA\Property(
            *      property="name",
            *      title="name",
            *      description="Company name",
            *      example="Optime"
            * )
            *
            * @var String
            */
            'name' => 'required|string|max:50',
            /**
            * @OA\Property(
            *      property="cnpj",
            *      title="cnpj",
            *      description="Company cnpj",
            *      example="77.344.421/0001-64"
            * )
            *
            * @var String
            */
            'cnpj' => ['required','string','max:20'],
            /**
             * @OA\Property(
             *      property="consortium",
             *      title="consortium",
             *      description="Company consortium",
             *      example="Rotas"
             * )
             *
             * @var String
             */
            'consortium' => 'required|string|max:50',
            /**
             * @OA\Property(
             *      property="phone",
             *      title="phone",
             *      description="Company phone",
             *      example="+5534998880101"
             * )
             *
             * @var String
             */
            'phone' => 'required|string|max:50',
                                    /**
             * @OA\Property(
             *      property="email",
             *      title="email",
             *      description="Company email",
             *      example="email@email.com"
             * )
             *
             * @var String
             */
            'email' => 'required|string|max:50',
                                    /**
             * @OA\Property(
             *      property="address",
             *      title="address",
             *      description="Company address",
             *      example="Rua Joaquim José, Número 69"
             * )
             *
             * @var String
             */
            'address' => 'required|string|max:255'
        ];
    }

    public function message()
    {
        return [

            'name.required'   => 'Required field',
            'name.string'   => 'Requires a string',
            'name.max'   => 'Maximum size 50',

            'consortium.required'   => 'Required field',
            'consortium.string'   => 'Requires a string',
            'consortium.max'   => 'Maximum size 50',

            'phone.required' => 'Required field',
            'phone.string'   => 'Requires a string',
            'phone.max'   => 'Maximum size 50',

            'email.required' => 'Required field',
            'email.string'   => 'Requires a string',
            'email.max'   => 'Maximum size 50',

            'address.required' => 'Required field',
            'address.string'   => 'Requires a string',
            'address.max'   => 'Maximum size 50',

            'cnpj.required'   => 'Required field',
            'cnpj.string'   => 'Requires a string',
            'cnpj.max'   => 'Maximum size 20',

        ];
    }
}
