<?php

namespace App\Http\Requests\api\ResourceVehicles;

use App\Rules\ThereIsEntity;
use Illuminate\Foundation\Http\FormRequest;

/**
 * @OA\Schema(
 *      title="Vehicle Request",
 *      description="Show Vehicle Request",
 *      type="object"
 * )
 */
class VehicleRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
        /**
        * @OA\Property(
        *      property="licencePlate",
        *      title="Licence Plate",
        *      description="Vehicle licence plate",
        *      example="PXL-5885"
        * )
        *
        * @var String
        */
        'licencePlate' => 'required|string',
        /**
        * @OA\Property(
        *      property="manufactureYear",
        *      title="Manufacture Year",
        *      description="Year the vehicle was manufactured",
        *      example=2021
        * )
        *
        * @var String
        */
        'manufactureYear' => 'required|numeric',
        /**
        * @OA\Property(
        *      property="prefix",
        *      title="Prefix",
        *      description="Vehicle prefix code",
        *      example="A582"
        * )
        *
        * @var String
        */
        'prefix' => 'required|string',
        /**
        * @OA\Property(
        *      property="seatedCapacity",
        *      title="Seated Capacity",
        *      description="Maximum of seated passagers",
        *      example="50"
        * )
        *
        * @var Integer
        */
        'seatedCapacity' => 'required|numeric',
        /**
        * @OA\Property(
        *      property="totalCapacity",
        *      title="Total Capacity",
        *      description="Maximum of passagers",
        *      example="120"
        * )
        *
        * @var Integer
        */
        'totalCapacity' => 'required|numeric',
        /**
        * @OA\Property(
        *      property="status",
        *      title="Status",
        *      description="Status of the vehicle",
        *      example="Active"
        * )
        *
        * @var String
        */
        'status' => 'required|string',
        /**
        * @OA\Property(
        *      property="vehicleType",
        *      title="Vehicle Type",
        *      description="Type of vehicle",
        *      example="Microonibus"
        * )
        *
        * @var String
        */
        'vehicleType' => 'required|string',
        /**
        * @OA\Property(
        *      property="hasElevator",
        *      title="Has Elevator",
        *      description="Indicates if the vehicle has handicappted elevator",
        *      example="true"
        * )
        *
        * @var Boolean
        */
        'hasElevator' => 'required|boolean',
        /**
        * @OA\Property(
        *      property="companyId",
        *      title="Company",
        *      description="Id of the company related to the vehicle",
        *      example="4f9495ab-9ba0-4a40-971a-936dfb11d21f"
        * )
        *
        * @var string
        */
        'companyId' => ['required','string', new ThereIsEntity("companies", "id")],
        ];
    }

    public function message()
    {
        return [

            'licencePlate.required'   => 'Required field',
            'licencePlate.string'   => 'Requires a string',

            'manufactureYear.required'   => 'Required field',
            'manufactureYear.integer'   => 'Requires a number',

            'prefix.required'   => 'Required field',
            'prefix.string'   => 'Requires a string',

            'seatedCapacity.required' => 'Required field',
            'seatedCapacity.integer'   => 'Requires a number',

            'totalCapacity.required' => 'Required field',
            'totalCapacity.integer'   => 'Requires a number',

            'status.required' => 'Required field',
            'status.string'   => 'Requires a string',

            'vehicleType.required' => 'Required field',
            'vehicleType.string'   => 'Requires a string',

            'hasElevator.required' => 'Required field',
            'hasElevator.boolean'   => 'Requires a true or false',

            'companyId.required' => 'Required field',
            'companyId.string'   => 'Requires a string',
        ];
    }
}
