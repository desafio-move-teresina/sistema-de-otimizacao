<?php

namespace App\Http\Requests\api\ResourceVehicles;

use App\Rules\ModelIntegrity;
use Illuminate\Foundation\Http\FormRequest;


class UpdateVehicleRequest extends FormRequest
{
      /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [

        ];
    }

    public function message()
    {
        return [

            'name.required'   => 'Required field',
            'name.string'   => 'Requires a string',

            'consortium.required'   => 'Required field',
            'consortium.string'   => 'Requires a string',

            'contact.required' => 'Required field',
            'contact.string'   => 'Requires a string',

        ];
    }
}
