<?php

namespace App\Http\Requests\api\ResourceConfiguration;

use Illuminate\Foundation\Http\FormRequest;

/**
 * @OA\Schema(
 *      title="Basic Rule Request",
 *      description="Store basic rule data",
 *      type="object"
 * )
 */

class BasicRuleRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            /**
             * @OA\Property(
             *      property="stopMinimumDistance",
             *      title="stopMinimumDistance",
             *      description="the bus must avoid stopping when going through adistance less than this (in meters)",
             *      example="300"
             * )
             *
             * @var number
             */
            'stopMinimumDistance'   => 'required|numeric',
            /**
             * @OA\Property(
             *      property="stopMaximumDistance",
             *      title="stopMaximumDistance",
             *      description="the bus must avoid traveling a distance greater than this without stopping (in meters)",
             *      example="500"
             * )
             *
             * @var number
             */
            'stopMaximumDistance'   => 'required|numeric',
            /**
             * @OA\Property(
             *      property="maximumWait",
             *      title="maximumWait",
             *      description="the passenger should not wait any longer than this time(in minutes)",
             *      example="300"
             * )
             *
             * @var number
             */
            'maximumWait' => 'required|numeric',
            /**
             * @OA\Property(
             *      property="defaultBusStopRadius",
             *      title="defaultBusStopRadius",
             *      description="default value of the radius at which the bus stop serves the population (in meters)",
             *      example="30"
             * )
             *
             * @var number
            */
            'defaultBusStopRadius'   => 'required|numeric',
        ];
    }

    public function messages()
    {
        return[

            'stopMinimumDistance.required'   => 'Required field',
            'stopMinimumDistance.numeric'   => 'Requires a number',

            'stopMaximumDistance.required'   => 'Required field',
            'stopMaximumDistance.numeric'   => 'Requires a number',

            'maximumWait.required' => 'Required field',
            'maximumWait.numeric'   => 'Requires a number',

            'defaultBusStopRadius.required'   => 'Required field',
            'defaultBusStopRadius.numeric'   => 'Requires a number',
        ];
    }
}
