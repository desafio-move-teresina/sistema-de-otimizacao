<?php

namespace App\Http\Requests\api\ResourceMap;

use Illuminate\Foundation\Http\FormRequest;

class MapEditorRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title'=>'required|string',
            
            'areaPoints'=>'array',
            'areaPoints.*.latitude' => 'required|numeric',
            'areaPoints.*.longitude' => 'required|numeric',

            'area'=>'required|numeric',

            'busStops'=>'array',
            'busStops.*.id'=>'string',
            'busStops.*.latitude'=>'required|numeric',
            'busStops.*.longitude'=>'required|numeric',
            'busStops.*.radius'=>'required|numeric',
            'busStops.*.direction'=>'required|numeric',
            'busStops.*.isActive'=>'required|boolean',
            'busStops.*.passengersIn'=>'required|numeric',
            'busStops.*.passengersOut'=>'required|numeric',
            'busStops.*.nearbyPlaces'=>'array',
            'busStops.*.nearbyPlaces.*.name'=>'required|String',
        ];
    }
}
