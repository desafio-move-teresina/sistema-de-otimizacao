<?php

namespace App\Http\Controllers\api\ResourceBusStop;

use App\Http\Controllers\Controller;
use App\Http\Traits\RouteTrait;
use App\Http\Traits\PointInPolygonTrait;
use Illuminate\Database\Eloquent\Collection;
use App\Models\api\ResourceMap\Map;
use App\Models\api\ResourceMap\BusStop;
use App\Models\api\ResourceMatrix\BusStopMatrix;
use App\Models\api\ResourceConfiguration\BasicRule;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;

class BusStopController extends Controller
{
    use RouteTrait, PointInPolygonTrait;

    public function generateBusStopMatrix($protocol)
    {
        $busStops = $this->getBusStopsFromMapArea(null, true);
        $path = base_path().'/engine/';

        $fpP = fopen($path."BusStopsFromProdater-$protocol.csv", 'w');
        $fpPol = fopen($path."MapPoligonInformation-$protocol.csv", 'w');

        foreach($busStops as $origin) {
            // Impressao do arquivo de paradas da Prodater
            fwrite($fpP, "$origin->prodater_code,$origin->latitude,$origin->longitude,$origin->fixed".PHP_EOL);
        }

        // Impressão do arquivo de pontos do polígono limítrofe para otimização
        $poligonArea = Map::orderBy('created_at', 'desc')->with('points')->first();
        fwrite($fpPol, "Map total area: $poligonArea->area".PHP_EOL);
        fwrite($fpPol, "Order,Latitude,Longitude".PHP_EOL);
        foreach($poligonArea->points as $point) {
            fwrite($fpPol, "$point->order,$point->latitude,$point->longitude".PHP_EOL);
        }
        
        fclose($fpPol);
        fclose($fpP);
    }

    /**
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     * 
     * @OA\Post(
     *      path="/api/getDistanceData",
     *      operationId="getDistanceData",
     *      tags={"Resource-BusStop"},
     *      summary="Gets the distance and duration from two bus stops",
     *      @OA\Parameter(
     *         name="X-Requested-With",
     *         in="header",
     *         description="Header",
     *         example="XMLHttpRequest",
     *         @OA\Schema(
     *             type="String"
     *         )
     *      ),
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(ref="#/components/schemas/BusStop")
     *      ),
     *      @OA\Response(
     *          response=422,
     *          description="Unprocessable Entity",
     *          @OA\JsonContent(
     *              type="object",
     *              @OA\Property(property="message", type="string"),
     *              @OA\Property(
     *                   property="errors",
     *                   type="array",
     *                   @OA\Items( @OA\Property(property="property",type="string"))
     *              )
     *          )
     *      ),
     * )
     */
    public function getBusStopsDistanceData(Request $request) {
        $busStops = json_decode($request->getContent());

        if (is_null($busStops)) {
            return Response::make("No data provided", 404);
        }

        $busStop = $busStops[0];

        $originStopCode      = $busStop->origin->code;
        $destinationStopCode = $busStop->destination->code;
        
        $busStopMatrix = BusStopMatrix::whereHas('originBusStop', function ($query) use ($originStopCode) {
            $query->where('prodater_code', $originStopCode);
        })->whereHas('destinationBusStop', function ($query) use ($destinationStopCode) {
            $query->where('prodater_code', $destinationStopCode);
        })->first();

        if (!$busStopMatrix) {
            $collection = collect([
                ["latitude" => $busStop->origin->latitude, "longitude" => $busStop->origin->longitude],
                ["latitude" => $busStop->destination->latitude, "longitude" => $busStop->destination->longitude]
            ]);
            
            $direction = json_decode($this->getBusStopsRouteArray($collection, false));
            $distanceValue = $direction->routes[0]->legs[0]->distance->value;
            $durationValue = $direction->routes[0]->legs[0]->duration->value;

            // Cria o registro no BD com as informações retornadas pelo google
            $busStopMatrix = BusStopMatrix::create([
                "origin_id" => BusStop::where('prodater_code', $originStopCode)->first()->id,
                "destination_id" => BusStop::where('prodater_code', $destinationStopCode)->first()->id,
                "origin_prodater_id" => $originStopCode,
                "destination_prodater_id" => $destinationStopCode,
                "distance" => $distanceValue,
                "duration" => $durationValue
            ]);
        }
        return Response::make($busStopMatrix, 201);
    }

    /* old bus stop matrix function - to be deleted
         public function generateBusStopMatrix()
    {
        $busStops = $this->getBusStopsFromMapArea(null, true);
        $fileDate = date("YmdHis");
        $path = base_path().'/engine/';

        // $fpD = fopen($path."BusStopsDistanceMatrix-$fileDate.csv", 'w');
        // $fpT = fopen($path."BusStopsDurationMatrix-$fileDate.csv", 'w');
        $fpP = fopen($path."BusStopsFromProdater-$fileDate.csv", 'w');
        $fpPol = fopen($path."MapPoligonInformation-$fileDate.csv", 'w');
        // $lineD = "";
        // $lineT = "";

        // $basicRule = BasicRule::orderByDesc('created_at')->first();
        // $minDistance = $basicRule->stopMinimumDistance;
        // $maxDistance = $basicRule->stopMaximumDistance;
        
        // create first line data
        // $lineD = ",";
        // foreach($busStops as $origin) {
        //     $lineD .= $origin->prodater_code.",";
        // }
        // $lineD = $lineT = rtrim($lineD, ",");
        // fwrite($fpT, $lineT.PHP_EOL);
        // fwrite($fpD, $lineD.PHP_EOL);

        foreach($busStops as $origin) {
            // $lineD = $lineT = $origin->prodater_code.",";

            // foreach($busStops as $destination) {
            //     $originStopCode = $origin->prodater_code;
            //     $destinationStopCode = $destination->prodater_code;
            //     if ($originStopCode == $destinationStopCode) {
            //         $lineD .= "0,";
            //         $lineT .= "0,";
            //         // Se o ponto for o mesmo, não calcula a distância
            //         continue;
            //     }

            //     // Usa a fórmula haversine para descobrir a distância entre 2 pontos
            //     $distance = $this->haversineGreatCircleDistance(
            //         $origin->latitude,
            //         $origin->longitude,
            //         $destination->latitude,
            //         $destination->longitude
            //     );

            //     // Se a distância for entre 300 e 500 metros, faz o cálculo da distância e tempo via Google
            //     // if ($distance > $minDistance && $distance < $maxDistance) {
            //     if (
            //         $origin->central_station || $destination->central_station ||
            //         ($distance > 100 && $distance < $maxDistance)
            //     ) {
            //         $distanceValue = 0;
            //         $durationValue = 0;

            //         $busStopMatrix = BusStopMatrix::whereHas('originBusStop', function ($query) use ($originStopCode) {
            //             $query->where('prodater_code', $originStopCode);
            //         })->whereHas('destinationBusStop', function ($query) use ($destinationStopCode) {
            //             $query->where('prodater_code', $destinationStopCode);
            //         })->first();

            //         if (!$busStopMatrix) {
            //             $collection = collect([
            //                 ["latitude" => $origin->latitude, "longitude" => $origin->longitude],
            //                 ["latitude" => $destination->latitude, "longitude" => $destination->longitude]
            //             ]);
            //             // $direction = json_decode($this->getBusStopsRouteArray($collection, false));
            //             $distanceValue = 100; //$direction->routes[0]->legs[0]->distance->value;
            //             $durationValue = 99; //$direction->routes[0]->legs[0]->duration->value;
 
            //             // Cria o registro no BD com as informações retornadas pelo google
            //             BusStopMatrix::create([
            //                 "origin_id" => BusStop::where('prodater_code', $originStopCode)->first()->id,
            //                 "destination_id" => BusStop::where('prodater_code', $destinationStopCode)->first()->id,
            //                 "origin_prodater_id" => $originStopCode,
            //                 "destination_prodater_id" => $destinationStopCode,
            //                 "distance" => $distanceValue,
            //                 "duration" => $durationValue
            //             ]);
            //         } else {
            //             $distanceValue = $busStopMatrix->distance;
            //             $durationValue = $busStopMatrix->duration;
            //         }
            //         $lineD .= $distanceValue.",";
            //         $lineT .= $durationValue.",";
            //     } else {
            //         $lineD .= "1000000,";
            //         $lineT .= "1000000,";
            //     }
            // }

            // Impressao do arquivo de paradas da Prodater
            fwrite($fpP, "$origin->prodater_code,$origin->latitude,$origin->longitude,$origin->fixed".PHP_EOL);
            // // Impressão do arquivo de matriz de distâncias
            // fwrite($fpD, rtrim($lineD, ",").PHP_EOL);
            // // Impressão do arquivo de matriz de tempo
            // fwrite($fpT, rtrim($lineT, ",").PHP_EOL);
        }

        // Impressão do arquivo de pontos do polígono limítrofe para otimização
        $poligonArea = Map::orderBy('created_at', 'desc')->with('points')->first();
        fwrite($fpPol, "Map total area: $poligonArea->area".PHP_EOL);
        fwrite($fpPol, "Order,Latitude,Longitude".PHP_EOL);
        foreach($poligonArea->points as $point) {
            fwrite($fpPol, "$point->order,$point->latitude,$point->longitude".PHP_EOL);
        }
        
        fclose($fpPol);
        fclose($fpP);
        // fclose($fpD);
        // fclose($fpT);
    }
     */

    private function getBusStopsFromProdater() {
        $client = new \GuzzleHttp\Client();
        $content = [];
        $page = 0;

        do {
            $res = $client->request(
                'GET',
                env('PRODATER_BASE_URL')."/paradas?size=3000&page=$page",
                [
                    'auth' => [
                        env('PRODATER_USER'),
                        env('PRODATER_PASSWORD')
                    ]
                ]
            );
            $content = array_merge($content, json_decode($res->getBody())->content);
            $page++;
        } while (json_decode($res->getBody())->totalPages > json_decode($res->getBody())->pageable->pageNumber+1);

        return $content;
    }

    /**
     * Calculates the great-circle distance between two points, with
     * the Haversine formula.
     * @param float $latitudeFrom Latitude of start point in [deg decimal]
     * @param float $longitudeFrom Longitude of start point in [deg decimal]
     * @param float $latitudeTo Latitude of target point in [deg decimal]
     * @param float $longitudeTo Longitude of target point in [deg decimal]
     * @param float $earthRadius Mean earth radius in [m]
     * @return float Distance between points in [m] (same as earthRadius)
     */
    private function haversineGreatCircleDistance($latitudeFrom, $longitudeFrom, $latitudeTo, $longitudeTo, $earthRadius = 6371000)
    {
        // convert from degrees to radians
        $latFrom = deg2rad($latitudeFrom);
        $lonFrom = deg2rad($longitudeFrom);
        $latTo = deg2rad($latitudeTo);
        $lonTo = deg2rad($longitudeTo);

        $latDelta = $latTo - $latFrom;
        $lonDelta = $lonTo - $lonFrom;

        $angle = 2 * asin(sqrt(pow(sin($latDelta / 2), 2) +
        cos($latFrom) * cos($latTo) * pow(sin($lonDelta / 2), 2)));
        return $angle * $earthRadius;
    }

    private function getBusStopsFromMapArea($polygon, $onlyActive) {
        $busStops = BusStop::when($onlyActive, function ($query) {
            return $query->where('isActive', true);
        })
        ->orderBy('central_station', 'desc')
        ->get();
        
        if (is_null($polygon)) {
            $areaPoints = Map::orderBy('created_at', 'desc')->first()->points;
            foreach ($areaPoints as $point) {
                $polygon[] = $point->latitude." ".$point->longitude;
            }
        }
        // The last point's coordinates must be the same as the first one's, to "close the loop"
        $polygon[] = $polygon[0]; // fechando o polígono

        $itemIndex = 0; // usado para reordenar as chaves
        foreach ($busStops as $key => $stop) {
            if (!$stop->central_station && $this->pointInPolygon("$stop->latitude $stop->longitude", $polygon) == "outside") {
               $busStops->forget($key);
               continue;
            }
            if ($key != $itemIndex) {
                $busStops[$itemIndex] = $stop;
                $busStops->forget($key);
            }
            $itemIndex++;
        }

        return $busStops;
    }

    /**
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     * 
     * @OA\Post(
     *      path="/api/getBusStopsInsidePolygon",
     *      operationId="getBusStopsInsidePolygon",
     *      tags={"Resource-Map"},
     *      summary="Retrieve all bus stops within the polygon",
     *      @OA\Parameter(
     *         name="X-Requested-With",
     *         in="header",
     *         description="Header",
     *         example="XMLHttpRequest",
     *         @OA\Schema(
     *             type="String"
     *         )
     *      ),
     *      @OA\Response(
     *          response=201,
     *          description="Successful operation",
     *          @OA\JsonContent(ref="#/components/schemas/BusStop")
     *       ),
     *      @OA\Response(
     *          response=422,
     *          description="Unprocessable Entity",
     *          @OA\JsonContent(
     *              type="object",
     *              @OA\Property(property="message", type="string"),
     *              @OA\Property(
     *                   property="errors",
     *                   type="array",
     *                   @OA\Items( @OA\Property(property="property",type="string"))
     *              )
     *          )
     *      ),
     * )
     */
    public function getBusStopsInsidePolygon(Request $request)
    {
        $vertices = json_decode($request->getContent());
        $polygon = [];

        foreach($vertices as $vertice) {
            $polygon[] = $vertice->latitude." ".$vertice->longitude;
        }

        return Response::make($this->getBusStopsFromMapArea($polygon, false), 201);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
