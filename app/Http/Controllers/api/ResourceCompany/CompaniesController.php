<?php

namespace App\Http\Controllers\api\ResourceCompany;

use App\Http\Controllers\Controller;
use App\Http\Requests\api\ResourceCompany\CompanyRequest;
use App\Http\Requests\api\ResourceCompany\UpdateCompanyRequest;
use App\Models\api\ResourceCompany\Company;
use Illuminate\Support\Facades\Response;
use Illuminate\Http\Request;

class CompaniesController extends Controller
{
    /**
     * @return \Illuminate\Http\Response
     * @OA\Get(
     *      path="/api/companies",
     *      operationId="getCompaniesList",
     *      tags={"Resource-Company"},
     *      summary="Get list of transportation companies",
     *      description="Returns a list of all companies",
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\JsonContent(
     *             type="object",
     *             @OA\Property(
     *                   property="companies",
     *                   type="array",
     *                   @OA\Items( @OA\Property( property="company",ref="#/components/schemas/Company"))
     *              )
     *          )
     *       )
     *     )
     */
    public function index()
    {
        return response()->json(Company::All(), 200);
    }

    /**
     * @param  \Illuminate\Http\CompanyRequest $request
     * @return \Illuminate\Http\Response
     * 
     * @OA\Post(
     *      path="/api/companies",
     *      operationId="storeCompanies",
     *      tags={"Resource-Company"},
     *      summary="Creates a new company",
     *      @OA\Parameter(
     *         name="X-Requested-With",
     *         in="header",
     *         description="Header",
     *         example="XMLHttpRequest",
     *         @OA\Schema(
     *             type="String"
     *         )
     *      ),
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(ref="#/components/schemas/CompanyRequest")
     *      ),
     *      @OA\Response(
     *          response=201,
     *          description="Successful operation",
     *          @OA\JsonContent(ref="#/components/schemas/Company")
     *       ),
     *      @OA\Response(
     *          response=422,
     *          description="Unprocessable Entity",
     *          @OA\JsonContent(
     *              type="object",
     *              @OA\Property(property="message", type="string"),
     *              @OA\Property(
     *                   property="errors",
     *                   type="array",
     *                   @OA\Items( @OA\Property(property="property",type="string"))
     *              )
     *          )
     *      ),
     * )
     */
    public function store(CompanyRequest $request)
    {
        $validated = $request->validated();

        $company = Company::create($validated);
    
        $url = env('APP_URL')."/api/companies/".$company['id'];
        return Response::make( $company, 201 )->header( 'Location', $url );
       
    }

    /**
     * @param  \App\Models\Company  $company
     * @return \Illuminate\Http\Response
     * @OA\Get(
     *      path="/api/companies/{id}",
     *      operationId="showCompanies",
     *      tags={"Resource-Company"},
     *      summary="Retrieve data from a single company",
     *      description="Returns all the data from a single company with its vehicles",
     *      @OA\Parameter(
     *          name="id",
     *          description="Company id",
     *          example="02031ea8-eac4-4757-b3c4-de8f66d4b357",
     *          in="path",
     *          @OA\Schema(
     *              type="String"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\JsonContent(ref="#/components/schemas/Company")
     *       ),
     *      @OA\Response(
     *          response=404,
     *          description="Company not found"
     *      )
     * )
     */
    public function show($company)
    {
        $response = Company::with("vehicles")->findOrFail($company);
        return response()->json( $response , 200);
    }

    /**
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Company  $company
     * @return \Illuminate\Http\Response
     * @OA\Put(
     *      path="/api/companies/{id}",
     *      operationId="updateCompanies",
     *      tags={"Resource-Company"},
     *      summary="Update the company's information",
     *      description="Update the company's information",
     *      @OA\Parameter(
     *         name="X-Requested-With",
     *         in="header",
     *         description="Header",
     *         example="XMLHttpRequest",
     *         @OA\Schema(
     *             type="String"
     *         )
     *      ),
     *      @OA\Parameter(
     *          name="id",
     *          description="Company id",
     *          example="02031ea8-eac4-4757-b3c4-de8f66d4b357",
     *          in="path",
     *          @OA\Schema(
     *              type="String"
     *          )
     *      ),
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(ref="#/components/schemas/UpdateCompanyRequest")
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\JsonContent(ref="#/components/schemas/Company")
     *       ),
     *      @OA\Response(
     *          response=404,
     *          description="Company not found"
     *       ),
     *      @OA\Response(
     *          response=422,
     *          description="Unprocessable Entity",
     *          @OA\JsonContent(
     *              type="object",
     *              @OA\Property(property="message", type="string"),
     *              @OA\Property(
     *                   property="errors",
     *                   type="array",
     *                   @OA\Items( @OA\Property(property="property",type="string"))
     *              )
     *          )
     *      )
     * )
     */
    public function update(UpdateCompanyRequest $request, $id)
    {
        $validated = $request->validated();

        $company = Company::find($id);

        if($company &&  $validated['cnpj'] == $company['cnpj']){

            $company->update($validated);

            return Response::make( $company, 200 );

        }else if(!$company){
            return Response::make( '', 404 );
        } 

        return response()->json("Unprocessable Entity: The cnpj field cannot be changed on this entity", 422);
    }

    /**
     * @param  \App\Models\Company  $company
     * @return \Illuminate\Http\Response

     * @OA\Delete(
     *      path="/api/companies/{id}",
     *      operationId="deleteCompanies",
     *      tags={"Resource-Company"},
     *      summary="Removes the company from the database",
     *      description="Carefull: This action removes the company from the database. This action cannot be undone.",
     *      @OA\Parameter(
     *          name="id",
     *          description="Company id",
     *          example="02031ea8-eac4-4757-b3c4-de8f66d4b357",
     *          in="path",
     *          @OA\Schema(
     *              type="String"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *       ),
     *      @OA\Response(
     *          response=404,
     *          description="Resource Not Found"
     *      )
     * )
     */
    public function destroy($company)
    {
        $response = Company::find($company);

        if($response){

            $response->delete();
            return Response::make( '', 200);
        }
        return Response::make( '', 404 );
        
    }
}
