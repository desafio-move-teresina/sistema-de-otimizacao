<?php

namespace App\Http\Controllers\api\ResourceMap;

use App\Http\Controllers\Controller;
use App\Http\Requests\api\ResourceMap\MapEditorRequest;
use App\Http\Response\api\ResourceMap\MapEditorResponse;
use App\Models\api\ResourceMap\BusStop;
use App\Models\api\ResourceMap\Map;
use App\Models\api\ResourceMap\Place;
use App\Models\api\ResourceMap\Point;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;

class MapEditorController extends Controller
{
    /**
     * @return \Illuminate\Http\Response
     * @OA\Get(
     *      path="/api/map-editor",
     *      operationId="getMapInformation",
     *      tags={"Resource-Map"},
     *      summary="Get data from the map",
     *      description="Returns the map info",
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\JsonContent(
     *             type="object",
     *             @OA\Property(
     *                   property="map_editor",
     *                   type="array",
     *                   @OA\Items( @OA\Property( property="mapEditor",ref="#/components/schemas/Map"))
     *              )
     *          )
     *       )
     *     )
     */
    public function index()
    {
        $map = Map::orderBy('created_at', 'desc')->first();

        if( $map ) {
            $mapEditor = new MapEditorResponse($map);
            return Response::make( $mapEditor->getResponse(), 200 );
        }
        return Response::make( [], 200 );

    }

    /**
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     * 
     * @OA\Post(
     *      path="/api/map-editor",
     *      operationId="storeMapInfo",
     *      tags={"Resource-Map"},
     *      summary="Register a new map info",
     *      @OA\Parameter(
     *         name="X-Requested-With",
     *         in="header",
     *         description="Header",
     *         example="XMLHttpRequest",
     *         @OA\Schema(
     *             type="String"
     *         )
     *      ),
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(ref="#/components/schemas/Map")
     *      ),
     *      @OA\Response(
     *          response=201,
     *          description="Successful operation",
     *          @OA\JsonContent(ref="#/components/schemas/Map")
     *       ),
     *      @OA\Response(
     *          response=422,
     *          description="Unprocessable Entity",
     *          @OA\JsonContent(
     *              type="object",
     *              @OA\Property(property="message", type="string"),
     *              @OA\Property(
     *                   property="errors",
     *                   type="array",
     *                   @OA\Items( @OA\Property(property="property",type="string"))
     *              )
     *          )
     *      ),
     * )
     */
    public function store(MapEditorRequest $request)
    { 
        $validated = $request->validated();
        
        $map = Map::create( $validated );

        if( isset( $validated['areaPoints'] ) ) {
            $pointOrder = 0;
            foreach( $validated['areaPoints'] as $areaPoint){
                $areaPoint['order'] = $pointOrder++;
                $point = Point::create($areaPoint);
                
                $map->points()->attach($point);
            }
        }

        if( isset( $validated['busStops'] ) ) {
            foreach( $validated['busStops'] as $busStopsRequest ) {
                $newBusStopCode = mt_rand(10000,100000);

                $busStop = BusStop::firstOrCreate(
                    [
                        'id'  => isset($busStopsRequest['id']) ? $busStopsRequest['id'] : "",
                    ],
                    [
                        'latitude'  => $busStopsRequest['latitude'],
                        'longitude' => $busStopsRequest['longitude'],
                        'radius' => $busStopsRequest['radius'],
                        'direction' => $busStopsRequest['direction'],
                        'isActive' => $busStopsRequest['isActive'],
                        'passengersIn' => $busStopsRequest['passengersIn'],
                        'passengersOut' => $busStopsRequest['passengersOut'],
                        "prodater_code" => $newBusStopCode,
                        "prodater_name" => "Nova parada $newBusStopCode",
                        "prodater_address" => ""
                    ]
                );
                if(!$busStop->wasRecentlyCreated){
                    if ($busStop->latitude != $busStopsRequest['latitude'])
                        $busStop->latitude = $busStopsRequest['latitude'];
                    if ($busStop->longitude != $busStopsRequest['longitude'])
                        $busStop->longitude = $busStopsRequest['longitude'];
                    if ($busStop->radius != $busStopsRequest['radius'])
                        $busStop->radius = $busStopsRequest['radius'];
                    if ($busStop->direction != $busStopsRequest['direction'])
                        $busStop->direction = $busStopsRequest['direction'];
                    if ($busStop->isActive != $busStopsRequest['isActive'])
                        $busStop->isActive = $busStopsRequest['isActive'];
                    if ($busStop->passengersIn != $busStopsRequest['passengersIn'])
                        $busStop->passengersIn = $busStopsRequest['passengersIn'];
                    if ($busStop->passengersOut != $busStopsRequest['passengersOut'])
                        $busStop->passengersOut = $busStopsRequest['passengersOut'];
                    
                    if ($busStop->isDirty()) {
                        $busStop->save();
                    }
                }
    
                if( isset( $busStopsRequest['nearbyPlaces'] ) ) {
                    foreach($busStopsRequest['nearbyPlaces'] as $nearbyPlace){            
                        Place::updateOrCreate(
                            [
                                'busStop'=> $busStop['id'],
                                'name'=>  $nearbyPlace['name'],
                            ]);
                    }
                }

                $map->busStops()->attach($busStop);
            }
        }

        $map['json_map'] = json_encode($validated);
        $map->update();

        $url = env('APP_URL')."/api/map-editor/".$map['id'];
        return Response::make( $validated, 201 )->header( 'Location', $url );
    }

    /**
     * @param  \App\Models\ResourceMap\MapEditor $mapEditor
     * @return \Illuminate\Http\Response
     * @OA\Get(
     *      path="/api/map-editor/{id}",
     *      operationId="showMapInfo",
     *      tags={"Resource-Map"},
     *      summary="Return map data for a single map",
     *      description="Returns Map data",
     *      @OA\Parameter(
     *          name="id",
     *          description="Map id",
     *          example="02031ea8-eac4-4757-b3c4-de8f66d4b357",
     *          in="path",
     *          @OA\Schema(
     *              type="String"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\JsonContent(ref="#/components/schemas/Map")
     *       ),
     *      @OA\Response(
     *          response=404,
     *          description="Error Not Found"
     *      )
     * )
     */
    public function show($mapEditor)
    {
        $map = Map::find($mapEditor)->first();

        if($map){

            $mapEditor = new MapEditorResponse($map);
            return Response::make( $mapEditor->getResponse(), 200 );
        }

        return Response::make( '', 404 ); 

    }
}
