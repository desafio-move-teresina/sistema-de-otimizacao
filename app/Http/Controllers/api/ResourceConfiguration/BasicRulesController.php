<?php

namespace App\Http\Controllers\api\ResourceConfiguration;

use App\Http\Controllers\Controller;
use App\Http\Requests\api\ResourceConfiguration\BasicRuleRequest;
use App\Models\api\ResourceConfiguration\BasicRule;
use Illuminate\Support\Facades\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class BasicRulesController extends Controller
{
    /**
     * @return \Illuminate\Http\Response
     * @OA\Get(
     *      path="/api/decision-variables",
     *      operationId="index",
     *      tags={"Decision-Variables"},
     *      summary="Retrieve basic rules data",
     *      description="Returns the basic rules for transportation planning",
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\JsonContent(
     *             type="object",
     *             @OA\Property(
     *                   property="basic_rules",
     *                   type="array",
     *                   @OA\Items( @OA\Property( property="basicRule",ref="#/components/schemas/BasicRule"))
     *              )
     *          )
     *       ),
     *     )
     *
     * Returns list of rules for optimization
     */
    public function index()
    {
        $basicRule = DB::table('basic_rules')
                ->orderBy('created_at', 'desc')
                ->first();

      
        return response()->json($basicRule, 200); 
    }

    /**
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     * 
     * @OA\Post(
     *      path="/api/decision-variables",
     *      operationId="storeProject",
     *      tags={"Decision-Variables"},
     *      summary="Create a new basic rule",
     *      @OA\Parameter(
     *         name="X-Requested-With",
     *         in="header",
     *         description="Request with data",
     *         example="XMLHttpRequest",
     *         @OA\Schema(
     *             type="String"
     *         )
     *      ),
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(ref="#/components/schemas/BasicRuleRequest")
     *      ),
     *      @OA\Response(
     *          response=201,
     *          description="Successful operation",
     *          @OA\JsonContent(ref="#/components/schemas/BasicRule")
     *       ),
     *      @OA\Response(
     *          response=422,
     *          description="Unprocessable Entity",
     *          @OA\JsonContent(
     *              type="object",
     *              @OA\Property(property="message", type="string"),
     *              @OA\Property(
     *                   property="errors",
     *                   type="array",
     *                   @OA\Items( @OA\Property(property="property",type="string"))
     *              )
     *          )
     *      ),
     * )
     */
    public function store(BasicRuleRequest $request)
    {
        $validated = $request->validated();
        $basicRule = BasicRule::create($validated);
        
        $url = env('APP_URL')."/api/basic-rule/".$basicRule['id'];
        return Response::make( $basicRule, 201 )->header( 'Location', $url );
    }

    /**
     * @param  \App\Models\BasicRule  $basicRule
     * @return \Illuminate\Http\Response
     *
     * @OA\Get(
     *      path="/api/decision-variables/{id}",
     *      operationId="getBasicRuleById",
     *      tags={"Decision-Variables"},
     *      summary="Get a single basic rule data",
     *      description="Returns the basic rule data",
     *      @OA\Parameter(
     *          name="id",
     *          description="Basic Rule id",
     *          example="02031ea8-eac4-4757-b3c4-de8f66d4b357",
     *          in="path",
     *          @OA\Schema(
     *              type="String"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\JsonContent(ref="#/components/schemas/BasicRule")
     *       ),
     *      @OA\Response(
     *          response=404,
     *          description="Error Not Found"
     *      )
     * )
    */
    public function show($basicRule)
    {
        $response = BasicRule::find($basicRule);

        if($response){
            return response()->json(  $response , 200);
        }
        return response()->json("Not found", 404);
    }
}
