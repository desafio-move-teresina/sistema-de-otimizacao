<?php

namespace App\Http\Controllers\api\ResourceRoute;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\api\ResourceRoute\Optimization;

class OptimizationController extends Controller
{
    /**
     * @return \Illuminate\Http\Response
     * @OA\Get(
     *      path="/api/optimization",
     *      operationId="getOptimizationsList",
     *      tags={"Resource-Engine"},
     *      summary="Get list of optimizations",
     *      description="Returns a list of all optimizations",
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\JsonContent(
     *             type="object",
     *             @OA\Property(
     *                   property="optimizations",
     *                   type="array",
     *                   @OA\Items( @OA\Property( property="company",ref="#/components/schemas/Optimization"))
     *              )
     *          )
     *       )
     *     )
     */
    public function index()
    {
        return response()->json(Optimization::orderBy('created_at', 'desc')->get(), 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
