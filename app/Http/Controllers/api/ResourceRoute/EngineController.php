<?php

namespace App\Http\Controllers\api\ResourceRoute;

use App\Http\Controllers\Controller;
use App\Http\Controllers\api\ResourceBusStop\BusStopController;
use App\Http\Traits\PointInPolygonTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use App\Http\Traits\RouteTrait;
use Symfony\Component\Process\Process;
use App\Models\api\ResourceRoute\Optimization;
use App\Models\api\ResourceMap\Map;
use App\Models\api\ResourceMap\BusStop;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Illuminate\Support\Facades\Log;

class EngineController extends Controller
{
    use RouteTrait, PointInPolygonTrait;

    /**
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     * 
     * @OA\Post(
     *      path="/api/engine",
     *      operationId="storeEngineData",
     *      tags={"Resource-Engine"},
     *      summary="Register new optimization data",
     *      description="Get the data returned by the optimization engine and inserts it into the database",
     *      @OA\Parameter(
     *         name="X-Requested-With",
     *         in="header",
     *         description="Header",
     *         example="XMLHttpRequest",
     *         @OA\Schema(
     *             type="String"
     *         )
     *      ),
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(ref="#/components/schemas/Route")
     *      ),
     *      @OA\Response(
     *          response=201,
     *          description="Successful operation",
     *          @OA\JsonContent(ref="#/components/schemas/Route")
     *       ),
     *      @OA\Response(
     *          response=422,
     *          description="Unprocessable Entity",
     *          @OA\JsonContent(
     *              type="object",
     *              @OA\Property(property="message", type="string"),
     *              @OA\Property(
     *                   property="errors",
     *                   type="array",
     *                   @OA\Items( @OA\Property(property="property",type="string"))
     *              )
     *          )
     *      ),
     * )
     */
    public function store(Request $request)
    {
        $data = json_decode(json_encode($request->all()));

        // cria o registro de otimização
        $newOptimization = Optimization::create([
            'name' => date("YmdHis")
        ]);

        // para cada rota retornada
        foreach($data->routes as $route) {
            $this->createNewRoute($newOptimization, $route);
        }
    }

    /**
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     * 
     * @OA\Post(
     *      path="/api/map-process",
     *      operationId="OptimizeMapRoutes",
     *      tags={"Resource-Engine"},
     *      summary="Optimize the routes for a given map area",
     *      description="Get the most recent map area and optimize the routes",
     *      @OA\Parameter(
     *         name="X-Requested-With",
     *         in="header",
     *         description="Header",
     *         example="XMLHttpRequest",
     *         @OA\Schema(
     *             type="String"
     *         )
     *      ),
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(ref="#/components/schemas/Route")
     *      ),
     *      @OA\Response(
     *          response=201,
     *          description="Successful operation",
     *          @OA\JsonContent(ref="#/components/schemas/Route")
     *       ),
     *      @OA\Response(
     *          response=422,
     *          description="Unprocessable Entity",
     *          @OA\JsonContent(
     *              type="object",
     *              @OA\Property(property="message", type="string"),
     *              @OA\Property(
     *                   property="errors",
     *                   type="array",
     *                   @OA\Items( @OA\Property(property="property",type="string"))
     *              )
     *          )
     *      ),
     * )
     */
    public function processMapRutes(Request $request) {

        $data = [];
        $data["routes"] = $request->routesCount;

        // Get the latest map area to be optimized
        $poligonArea = Map::orderBy('created_at', 'desc')->with('points')->first();
        $vertices = [];
        foreach($poligonArea->points as $point) {
            $vertices[] = ["lat" => $point->latitude, "long" => $point->longitude];
        }
        $data["polygon"] = $vertices;

        $busStops = $this->getBusStopsFromVertices($vertices, true);
        $stops = [];
        foreach($busStops as $origin) {
            // Impressao do arquivo de paradas da Prodater
            $stops[] = [
                "id" => $origin->prodater_code, 
                "lat" => $origin->latitude, 
                "long" => $origin->longitude, 
                "fixed" => $origin->fixed, 
            ];
        }

        $data["busStops"] = $stops;
        
        try {
            $client = new \GuzzleHttp\Client();
            $response = $client->request('POST', env("ENGINE_URL"), [
                'json' => $data
            ]);

            return Response::make( $data, $response->getStatusCode() );
        } catch (\GuzzleHttp\Exception\ConnectException $e) {
            return Response::make( "The engine service seems to be down", 500 );
        }

        return Response::make( "Ok", 201 );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return null;
    }

    private function getBusStopsFromVertices($vertices, $onlyActive) {
        $busStops = BusStop::when($onlyActive, function ($query) {
            return $query->where('isActive', true);
        })
        ->orderBy('central_station', 'desc')
        ->get();
        
        foreach ($vertices as $vertice) {
            $polygon[] = $vertice["lat"]." ".$vertice["long"];
        }
        // The last point's coordinates must be the same as the first one's, to "close the loop"
        $polygon[] = $polygon[0]; // fechando o polígono

        $itemIndex = 0; // usado para reordenar as chaves
        foreach ($busStops as $key => $stop) {
            if (!$stop->central_station && $this->pointInPolygon("$stop->latitude $stop->longitude", $polygon) == "outside") {
               $busStops->forget($key);
               continue;
            }
            if ($key != $itemIndex) {
                $busStops[$itemIndex] = $stop;
                $busStops->forget($key);
            }
            $itemIndex++;
        }

        return $busStops;
    }
}
