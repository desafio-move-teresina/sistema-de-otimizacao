<?php

namespace App\Http\Controllers\api\ResourceRoute;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\api\ResourceRoute\Route;
use App\Models\api\ResourceMap\BusStop;
use App\Http\Traits\RouteTrait;

class RoutesController extends Controller
{
    use RouteTrait;

    /**
     * @return \Illuminate\Http\Response
     * @OA\Get(
     *      path="/api/routes",
     *      operationId="getRouteList",
     *      tags={"Resource-Route"},
     *      summary="Get list of routes",
     *      description="Returns list of all the routes or filtered by id",
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\JsonContent(
     *             type="object",
     *             @OA\Property(
     *                   property="routes",
     *                   type="array",
     *                   @OA\Items( @OA\Property( property="route",ref="#/components/schemas/Route"))
     *              )
     *          )
     *       )
     *     )
     */
    public function index(Request $request)
    {
        $routes = Route::with(['routevehicles', 'routelegs.steps', 'busstops'])->where(function ($query) use ($request) {
            if (! is_null($request->route)) {
                $query->where('id', $request->route);
            }
            if (! is_null($request->optimizationId)) {
                $query->where('optimizationId', $request->optimizationId);
            }
        })->get();

        return $routes;
    }

    /**
     * @param  \App\Models\ResourceRoute $route
     * @return \Illuminate\Http\Response
     * @OA\Get(
     *      path="/api/routes/{id}",
     *      operationId="showRoutes",
     *      tags={"Resource-Route"},
     *      summary="Gets all the data for a single route",
     *      description="Returns the route, its legs, steps, suggested vehicles and bus stops",
     *      @OA\Parameter(
     *          name="id",
     *          description="Route id",
     *          example="02031ea8-eac4-4757-b3c4-de8f66d4b357",
     *          in="path",
     *          @OA\Schema(
     *              type="String"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\JsonContent(ref="#/components/schemas/Route")
     *       ),
     *      @OA\Response(
     *          response=404,
     *          description="Error Not Found"
     *      )
     * )
     */
    public function show($route)
    {
        // $response = Route::with(['routevehicles', 'routelegs.steps', 'busstops'])->findOrFail($route);
        $response = Route::with(['routelegs.steps', 'busstops'])->findOrFail($route);
        return response()->json( $response , 200);

        // return $routes;
    }

    /**
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     * @OA\Get(
     *      path="/api/reorderSingleRoute/{route}",
     *      operationId="reorderSingleRoute",
     *      tags={"Resource-Route"},
     *      summary="Reorders the route when any bus stop have been innactivated",
     *      description="Re-optimize the given route replacing innactivated bus stops",
     *      @OA\Parameter(
     *          name="route",
     *          description="Route id",
     *          example="02031ea8-eac4-4757-b3c4-de8f66d4b357",
     *          in="path",
     *          @OA\Schema(
     *              type="String"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\JsonContent(ref="#/components/schemas/Route")
     *       ),
     *      @OA\Response(
     *          response=404,
     *          description="Error Not Found"
     *      )
     * )
     */
    public function reorderSingleRoute(Request $request) {
        $route = Route::with('busstops')->findOrFail($request->route);
        $updated = false;

        // $busStops = BusStop
        foreach($route->busStops as $busStop) {
            if (!$busStop->isActive) {
                $updated = true;
                $range = 0.001;
                // Ponto foi desativado, troca por outro ponto
                do {
                    // capturo primeira parada ativa próxima ao ponto desativado
                    $newBusStop = BusStop::
                        whereBetween('latitude', [$busStop->latitude - $range, $busStop->latitude + $range])->
                        whereBetween('longitude', [$busStop->longitude - $range, $busStop->longitude + $range])->
                        where('isActive', 1)->
                        first();

                    if (!is_null($newBusStop)) {
                        // Encontrando uma parada próxima, atualiza para novo local
                        $busStop->pivot->busStopId = $newBusStop->id;
                        $busStop->pivot->save();
                        break;
                    }

                    // se não achou parada, aumenta o raio de pesquisa e faz nova consulta
                    $range *= 2;
                } while ($range < 1);
            }
        }

        // Faz a atualização dos pontos e rotas
        if($updated) $this->updateRoute($route);

        return $this->show($route->id);
    }

}
