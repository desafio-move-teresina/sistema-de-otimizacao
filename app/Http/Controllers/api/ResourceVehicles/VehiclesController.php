<?php

namespace App\Http\Controllers\api\ResourceVehicles;

use App\Http\Controllers\Controller;
use App\Http\Requests\api\ResourceVehicles\VehicleRequest;
use App\Models\api\ResourceVehicles\Vehicle;
use Illuminate\Support\Facades\Response;

use Illuminate\Http\Request;


class VehiclesController extends Controller
{
    /**
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     * @OA\Get(
     *      path="/api/vehicles",
     *      operationId="getVehicleList",
     *      tags={"Resource-Vehicle"},
     *      summary="Get list of vehicles",
     *      description="Returns list of all the vehicles or filtered by company",
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\JsonContent(
     *             type="object",
     *             @OA\Property(
     *                   property="vehicle",
     *                   type="array",
     *                   @OA\Items( @OA\Property( property="vehicle",ref="#/components/schemas/Vehicle"))
     *              )
     *          )
     *       )
     *     )
     */
    public function index(Request $request)
    {
        $vehicles = Vehicle::with(['company'])->where(function ($query) use ($request) {
            if (! empty($request->company)) {
                $query->where('companyId', $request->company);
            }
    
            if (! empty($request->status)) {
                $query->where('status', $request->status);
            }
        })->get();

        return $vehicles;
    }

    /**
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     * 
     * @OA\Post(
     *      path="/api/vehicles",
     *      operationId="storeVehicle",
     *      tags={"Resource-Vehicle"},
     *      summary="Create a new vehicle",
     *      description="Register a new vehicle for a given company",
     *      @OA\Parameter(
     *         name="X-Requested-With",
     *         in="header",
     *         description="Header",
     *         example="XMLHttpRequest",
     *         @OA\Schema(
     *             type="String"
     *         )
     *      ),
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(ref="#/components/schemas/VehicleRequest")
     *      ),
     *      @OA\Response(
     *          response=201,
     *          description="Successful operation"
     *       ),
     *      @OA\Response(
     *          response=422,
     *          description="Unprocessable Entity",
     *          @OA\JsonContent(
     *              type="object",
     *              @OA\Property(property="message", type="string"),
     *              @OA\Property(
     *                   property="errors",
     *                   type="array",
     *                   @OA\Items( @OA\Property(property="property",type="string"))
     *              )
     *          )
     *      ),
     * )
     */
    public function store(VehicleRequest $request)
    {
        $validated = $request->validated();
        
        $vehicle = Vehicle::create($validated);
    
        $url = env('APP_URL')."/api/vehicles/".$vehicle['id'];
        return Response::make( '', 201 )->header( 'Location', $url );
    }

    /**
     * @param  \App\Models\Vehicle  $vehicle
     * @return \Illuminate\Http\Response
     * @OA\Get(
     *      path="/api/vehicles/{id}",
     *      operationId="showVehicle",
     *      tags={"Resource-Vehicle"},
     *      summary="Get the data for a single vehicle",
     *      description="Returns full vehicle and company data",
     *      @OA\Parameter(
     *          name="id",
     *          description="Vehicle id",
     *          example="02031ea8-eac4-4757-b3c4-de8f66d4b357",
     *          in="path",
     *          @OA\Schema(
     *              type="String"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\JsonContent(ref="#/components/schemas/Vehicle")
     *       ),
     *      @OA\Response(
     *          response=404,
     *          description="Error Not Found"
     *      )
     * )
     */
    public function show($vehicle)
    {
        $response = Vehicle::with(['company'])->findOrFail($vehicle);
        return response()->json( $response , 200);
    }

    /**
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\Vehicle $vehicle
     * @return \Illuminate\Http\Response
     * @OA\Put(
     *      path="/api/vehicles/{id}",
     *      operationId="updateVehicle",
     *      tags={"Resource-Vehicle"},
     *      summary="Update vehicle data",
     *      description="Update Vehicle data",
     *      @OA\Parameter(
     *         name="X-Requested-With",
     *         in="header",
     *         description="Header",
     *         example="XMLHttpRequest",
     *         @OA\Schema(
     *             type="String"
     *         )
     *      ),
     *      @OA\Parameter(
     *          name="id",
     *          description="Vehicle id",
     *          example="02031ea8-eac4-4757-b3c4-de8f66d4b357",
     *          in="path",
     *          @OA\Schema(
     *              type="String"
     *          )
     *      ),
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(ref="#/components/schemas/VehicleRequest")
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation"
     *       ),
     *      @OA\Response(
     *          response=404,
     *          description="Not Found"
     *       ),
     *      @OA\Response(
     *          response=422,
     *          description="Unprocessable Entity",
     *          @OA\JsonContent(
     *              type="object",
     *              @OA\Property(property="message", type="string"),
     *              @OA\Property(
     *                   property="errors",
     *                   type="array",
     *                   @OA\Items( @OA\Property(property="property",type="string"))
     *              )
     *          )
     *      )
     * )
     */
    public function update(VehicleRequest $request, $id)
    {
        $validated = $request->validated();

        $vehicle = Vehicle::findOrFail($id);
        $vehicle->update($validated);

        return Response::make( '', 200 );
    }

    /**
     * @param  \App\Models\Vehicle  $vehicle
     * @return \Illuminate\Http\Response

     * @OA\Delete(
     *      path="/api/vehicles/{id}",
     *      operationId="deleteVehicle",
     *      tags={"Resource-Vehicle"},
     *      summary="Removes the vehicle from the database",
     *      description="Carefull: This action removes the vehicle from the database. This action cannot be undone.",
     *      @OA\Parameter(
     *          name="id",
     *          description="Vehicle id",
     *          example="02031ea8-eac4-4757-b3c4-de8f66d4b357",
     *          in="path",
     *          @OA\Schema(
     *              type="String"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *       ),
     *      @OA\Response(
     *          response=404,
     *          description="Resource Not Found"
     *      )
     * )
     */
    public function destroy($vehicle)
    {
        $response = Vehicle::findOrFail($vehicle);

        $response->delete();
        return Response::make( '', 200);
    }
}
