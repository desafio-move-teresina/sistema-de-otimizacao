<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
//php artisan l5-swagger:generate
/**
 * @OA\Info(
 *      version="1.0.0",
 *      title="Public transportation route optimizer",
 *      description="This optimizer was built by OpTime (<a href='http://www.optimerotas.com.br'>www.optimerotas.com.br</a>) as a Proof of Concept to the City of Teresina - Piauí - Brazil.<br>For more information on how to install and run, take a look at the <a href='https://bitbucket.org/desafio-move-teresina/sistema-de-otimizacao/src/master/README.md'>ReadMe page</a>.",
 *      @OA\Contact(
 *          email="chacon@optimerotas.com.br"
 *      ),
 *      @OA\License(
 *          name="Apache 2.0",
 *          url="http://www.apache.org/licenses/LICENSE-2.0.html"
 *      )
 * )
*/

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
}
