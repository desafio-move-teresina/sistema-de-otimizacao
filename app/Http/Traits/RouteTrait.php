<?php

namespace App\Http\Traits;

use App\Models\api\ResourceMap\BusStop;
use App\Models\api\ResourceRoute\Optimization;
use App\Models\api\ResourceRoute\Route;
use App\Models\api\ResourceRoute\RouteLeg;
use App\Models\api\ResourceRoute\Step;
use App\Models\api\ResourceRoute\RouteVehicle;
use App\Models\api\ResourceVehicles\Vehicle;
use \JsonMachine\JsonMachine;

trait RouteTrait {
    public function createNewRoute($newOptimization, $route) {
        $busStops = [];
        $direction = "";
        // cria o array de dados de parada de onibus

        foreach($route->busStops as $busStop) {
            $busStops[] = $busStop->code;
        }
        $placeholders = implode(',',array_fill(0, count($busStops), '?')); // string for the query
        $busStops = BusStop::whereIn('prodater_code', $busStops)
            ->orderByRaw("field(prodater_code,{$placeholders})", $busStops)->get();

        // busca o retorno Directions do Google Maps
        $directions = $this->getBusStopsRoute(clone $busStops);

        ////// completa os dados da polyline
        $polylineArray = [];
        foreach ($directions as $direction) {
            try {
                // It is necessary to use foreach to reach the information inside JsonMachine
                foreach(JsonMachine::fromString($direction, '/routes/0/overview_polyline') as $polyline) {
                    $polylineArray = array_merge($polylineArray, \GeometryLibrary\PolyUtil::decode($polyline));
                }
            } catch (\JsonMachine\Exception\PathNotFoundException $e) {
                // $overviewPolyline = '';
            }
        }
        $overviewPolyline = \GeometryLibrary\PolyUtil::encode($polylineArray);
        
        /////// completa os dados da rota
        $legsDistance = 0;
        $legsDuration = 0;
        foreach ($directions as $direction) {
            foreach(JsonMachine::fromString($direction, '/routes/0/legs') as $legs) {
                $legsDistance += $legs["distance"]["value"];
                $legsDuration += $legs["duration"]["value"];
            }
        }
        $newRoute = Route::create([
            'name' => 'Rota '.mt_rand(1,99999),
            'estimatedPassengers' => 0, //$route['estimatedPassengers'],
            'estimatedDistance' => $legsDistance,
            'estimatedDuration' => $legsDuration,
            'overviewPolyline' => $overviewPolyline,
            'optimizationId' => $newOptimization->id
        ]);

        ////// completa os dados das legs e steps
        foreach ($directions as $direction) {
            foreach(JsonMachine::fromString($direction, '/routes/0/legs') as $legKey => $leg) {
                $routeLeg = RouteLeg::create([
                    'distance' => $leg['distance']['value'],
                    'duration' => $leg['duration']['value'],
                    'routeId' => $newRoute->id,
                ]);

                // Creating the Leg steps
                foreach(JsonMachine::fromString($direction, '/routes/0/legs/'.$legKey.'/steps') as $step) {
                    $routeStep = Step::create([
                        'distance' => $step['distance']['value'],
                        'duration' => $step['duration']['value'],
                        'htmlInstruction' => $step['html_instructions'],
                        'polyline' => $step['polyline']['points'],
                        'maneuver' => isset($step['maneuver']) ? $step['maneuver'] : null,
                        'routeLegId' => $routeLeg->id,
                    ]);
                }
            }
        }

        // para cada parada
        // cria a parada associada à rota
        foreach($busStops as $busStop) {
            $newRoute->busStops()->attach($busStop->id);
        }
    }

    public function updateRoute(Route $route) {
        // busca o retorno Directions do Google Maps
        $direction = $this->getBusStopsRoute($route->busStops);
        
        // It is necessary to use foreach to reach the information inside JsonMachine
        foreach(JsonMachine::fromString($direction, '/routes/0/overview_polyline') as $polyline) {
            $route->overviewPolyline = $polyline;
            $route->save();
        }

        // Apaga os registros de legs e steps para refazer a rota
        foreach($route->routeLegs as $routeLeg) {
            $routeLeg->steps()->delete();            
        }
        $route->routeLegs()->delete();

        // Creating the Route Legs
        foreach(JsonMachine::fromString($direction, '/routes/0/legs') as $legKey => $leg) {
            $routeLeg = RouteLeg::create([
                'distance' => $leg['distance']['value'],
                'duration' => $leg['duration']['value'],
                'routeId' => $route->id,
            ]);

            // Creating the Leg steps
            foreach(JsonMachine::fromString($direction, '/routes/0/legs/'.$legKey.'/steps') as $step) {
                $routeStep = Step:: create([
                    'distance' => $step['distance']['value'],
                    'duration' => $step['duration']['value'],
                    'htmlInstruction' => $step['html_instructions'],
                    'polyline' => $step['polyline']['points'],
                    'maneuver' => isset($step['maneuver']) ? $step['maneuver'] : null,
                    'routeLegId' => $routeLeg->id,
                ]);
            }
        }
    }

    private function getBusStopsRoute($busStops, $roundTrip = true) {
        $data = [];

        while (($spliceData = $busStops->splice(0, 24))->count() > 0) {
            $data[] = $this->getRoutesFromGoogle($spliceData, $roundTrip);
        }

        return $data;
    }

    private function getRoutesFromGoogle($busStops, $roundTrip = false) {
        $origin = $busStops->shift();
        $destination = $roundTrip ? $origin : $busStops->pop();
        $waypoints = "";

        foreach($busStops as $busStop) {
            $waypoints .= $busStop->latitude . "," . $busStop->longitude . "|";
        }
        $waypoints = trim($waypoints, "|");

        $data = \GoogleMaps::load('directions')->setParam([
            'origin' => $origin->latitude . "," . $origin->longitude,
            'destination' => $destination->latitude . "," . $destination->longitude,
            'language' => 'pt-BR',
            'mode' => 'driving',
            'waypoints' => $waypoints,
        ])->get();

        return $data;
    }

    private function getBusStopsRouteArray($busStops, $roundTrip = true) {
        $origin = $busStops->shift();
        $destination = ($roundTrip) ? $origin : $busStops->pop();
        $waypoints = "";

        foreach($busStops as $busStop) {
            $waypoints .= $busStop["latitude"] . "," . $busStop["longitude"] . "|";
        }
        $waypoints = trim($waypoints, "|");

        $data = \GoogleMaps::load('directions')->setParam([
            'origin' => $origin["latitude"] . "," . $origin["longitude"],
            'destination' => $destination["latitude"] . "," . $destination["longitude"],
            'language' => 'pt-BR',
            'mode' => 'driving',
            'waypoints' => $waypoints,
        ])->get();

        return $data;
    }
}