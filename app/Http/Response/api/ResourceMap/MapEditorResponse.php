<?php

namespace App\Http\Response\api\ResourceMap;

use App\Models\api\ResourceMap\Map;
use App\Models\api\ResourceMap\BusStop;
use App\Http\Traits\PointInPolygonTrait;

class MapEditorResponse{

    use PointInPolygonTrait;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    private $id; 
    private $title; 
    private $areaPoints;
    private $busStops;

    public function __construct( Map $map)
    {
        $polygon = [];
        $this->id = $map->id;
        $this->title = $map->title;
        $this->areaPoints = $map->points()->get();
        $busStops = BusStop::get();

        if ($this->areaPoints->count() > 0) {
            foreach ($this->areaPoints as $point) {
                $polygon[] = $point->latitude." ".$point->longitude;
            }
            // The last point's coordinates must be the same as the first one's, to "close the loop"
            $polygon[] = $polygon[0]; // fechando o polígono
        }

        $itemIndex = 0; // usado para reordenar as chaves
        foreach ($busStops as $key => $stop) {
            if ($this->pointInPolygon("$stop->latitude $stop->longitude", $polygon) == "outside") {
               $busStops->forget($key);
               continue;
            }
            if ($key != $itemIndex) {
                $busStops[$itemIndex] = $stop;
                $busStops->forget($key);
            }
            $itemIndex++;
        }

        $this->busStops = $busStops;
    }

    public function getResponse(){
        return [
            'id'=>$this->id,
            'title'=>$this->title,
            'areaPoints'=>$this->areaPoints,
            'busStops'=>$this->busStops,
        ];
    }
}