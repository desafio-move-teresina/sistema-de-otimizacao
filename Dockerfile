FROM php:7.3
RUN apt-get update -y && apt-get install -y && apt-get install python3-pip -y && python3 -m pip install numpy && python3 -m pip install Shapely && python3 -m pip install requests && python3 -m pip install matplotlib && python3 -m pip install tdqm && python3 -m pip install networkx
RUN apt-get update -y && apt-get install -y openssl zip unzip git netcat
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer
RUN docker-php-ext-install pdo mbstring mysqli pdo_mysql
WORKDIR /app
COPY . /app
RUN composer install