from ctypes import pythonapi
import pickle
import sys
import numpy as np
from numpy.lib.npyio import save
from shapely.geometry import Polygon
import math 
import sys
import requests
import json
import csv
import codecs
import matplotlib.pyplot as plt
import matplotlib as pltColor
from tqdm import tqdm
import copy
import networkx as nx
import random

# protocolo-20211114205525
# quantidade de rotas - 

URL = 'http://back-end:8181'
# URL = 'http://localhost:8082'
client = requests.session()

NUMBER_OF_CITIES = 0
NUMBER_OF_TRAVELING_SALESMAN = int(sys.argv[2])
NUMBER_OF_ITERATIONS = 10
POPULATION_SIZE = 10
data = []

def decision_variables():
    decision_variables_request = requests.get(url=URL+"/api/decision-variables", headers=dict(Referer=URL))
    decision_variables = decision_variables_request.json()
    return {
        "stopMinimumDistance": decision_variables['stopMinimumDistance'],
        "stopMaximumDistance": decision_variables['stopMaximumDistance'],
        "maximumWait": decision_variables['stopMinimumDistance'],
        "defaultBusStopRadius": decision_variables['defaultBusStopRadius'],
    }

def get_distance_data(code_Origin, latitude_Origin, longitude_Origin, code_Destination, latitude_Destination, longitude_Destination):

    point = [{
        "origin": {
            "code" : code_Origin,
            "latitude" : latitude_Origin,
            "longitude" : longitude_Origin
        },
        "destination" : {
            "code" : code_Destination,
            "latitude" : latitude_Destination,
            "longitude" : longitude_Destination
        }
    }]

    get_distance_data_request = requests.post(url=URL+"/api/getDistanceData",data=json.dumps(point),headers=dict(Referer=URL))
    get_distance_data = get_distance_data_request.json()

    return {
        "origin_prodater_id": get_distance_data['origin_prodater_id'],
        "destination_prodater_id": get_distance_data['destination_prodater_id'],
        "distance": get_distance_data['distance'],
        "duration": get_distance_data['duration'],
    }

def salve_results(results):

    rotas = []
    for i in range(len(results)):
        busStop = []
        for j in range(len(results[i])):
            code = {
                "code": results[i][j]
            }
            busStop.append(code) 
        busStops = {
            "busStops": busStop
        }
        rotas.append(busStops)

    body = {
        "routes": rotas
    }
    requests.post(url=URL+"/api/engine", json=body, headers=dict(Referer=URL))


def get_cluster():

    protocol_number = sys.argv[1]
    return np.array(np.genfromtxt('../engine/BusStopsFromProdater-'+protocol_number+'.csv',delimiter=","))
   # protocol_number = sys.argv[1]

    '''with open('data/BusStopsFromProdater-20211101170114.csv', 'rb') as ficheiro:
        reader = csv.reader(codecs.iterdecode(ficheiro, 'utf-8'))

        csv_matrix = []
        for row in reader:
            csv_linha = []
            index = 0
            for information in row:
                if(index != 0):
                    csv_linha.append( float(information) )
                index=index + 1
            csv_matrix.append(csv_linha)

    return np.array(csv_matrix)'''



def get_poligon():

    protocol_number = sys.argv[1]

    with open('../engine/MapPoligonInformation-'+protocol_number+'.csv', 'rb') as ficheiro:
        reader = csv.reader(codecs.iterdecode(ficheiro, 'utf-8'))

        csv_matrix = []
        index_matrix = 0
        for row in reader:
            if(index_matrix > 1):
                csv_linha = []
                index_line = 0
                for information in row:
                    if(index_line != 0):
                        csv_linha.append( float(information) )
                    index_line=index_line + 1
                csv_matrix.append(csv_linha)
            index_matrix = index_matrix+1
        
    return np.array(csv_matrix)

# %%
def kmean_cobertura(C, F, k , flag):

    aux = np.mean(C,0)

    n=0
    if( np.size(C,0) != 0):
        n = np.size(C,0)

    r=0
    if( np.size(F,0) != 0):
        r = np.size(F,0)

    if r == 0:
        F = np.empty(shape=(0, 2))
    if r == k:
        cluster = F
    else:
        #aux_pos = randperm(n-r,k-r);
        #cluster = [F;C(aux_pos,:)];
        arr=np.random.permutation( (n) - r) + 1
        cluster_aux = []
        for i in range(k-r):
            cluster_aux.append( C[ arr[i] ] )
        cluster = np.concatenate( (F,np.array(cluster_aux) ) )
    
    # % Inicia as cluster de forma aleat�ria,
    # % colocando os pontos fixos no in�cio da cluster

    cluster_anterior = np.zeros(shape=(k,2))
    Pos = {}
    it = 0

    while( sum(sum(abs(cluster_anterior - cluster))) > 10**(-8) ) :
        it = it + 1
        # %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%    
        # % matrix de dist�ncia entre pontos de duas listas
        D = []
        C_max = np.size(C,0)
        for C_i in range( 0 , C_max ):
            cluster_j_max = np.size(cluster,0)
            linha = []
            for cluster_j in range( 0 , cluster_j_max ):
                distancia = math.dist( C[C_i], cluster[cluster_j])
                linha.append(distancia)
            D.append(linha)
    
        D = np.array(D)

        # %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%    
        # % Encontrando pos cluster
        pos_cluster = []
        D_i_max = np.size(D,0)
        for D_i in range( 0 , D_i_max ):
            minValue = np.amin(D[D_i])
            minIndex = np.where( D[D_i] == minValue)
            pos_cluster.append( minIndex[0][0] )
        
        pos_cluster = np.array(pos_cluster)
        cluster_anterior = np.copy(cluster)        
        # %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%    
        # % Encontrando e reposicionando os centros
        for i in range(0,k):
            aux_pos = np.where( pos_cluster == i)

            # % matriz de vetores dentro de uma classe  
            C_pos = C[aux_pos] 
            media = np.mean(C_pos,0)
            Pos[i] = aux_pos
            
            if i > r :
                
                if math.isnan(sum(media)):
                    cluster[i] = np.random.rand()*aux
                else :
                    cluster[i] = media
    
        cluster_real = np.copy(cluster)
        if flag == 2 :
            print("chegou aqui")
            # cluster = separa_cluster(C,cluster)

    return {
        "cluster": cluster,
        "D": D,
        "Pos":Pos,
        "cluster_real": cluster_real,
        "it":it
    }

# function [cluster,d_min,pos_cluster] = separa_cluster(X,C) % encontra o ponto de onibus mais próximo da solução do kmeans

#     D = dist(C,X');
#     [d_min, pos_cluster] = min(D,[],2);
#     cluster = X(pos_cluster,:);

def separa_cluster(C,X,P):

    D = matrix_distance(C,X)
    pos_cluster = np.argmin(D,axis=1)
    cluster = C[pos_cluster]

    #grafico(C,X,cluster,P)

    return {
        "cluster":np.array(cluster),
        "pos_cluster":np.array(pos_cluster)
    }

def matrix_distance(A,B):
    dist = lambda p1, p2: math.sqrt(((p1-p2)**2).sum())
    DM = np.asarray([[dist(p1, p2) for p2 in A] for p1 in B])
    return DM

def grafico(C,X,cluster,P):

    x_1, y_1 = C.T
    x_2, y_2 = X.T
    x_3, y_3 = cluster.T

    plt.gca().add_patch(plt.Polygon(P, fill = None, lw = 3., ls = 'dotted', edgecolor = 'k'))
    plt.scatter(x_1, y_1,color='red')
    plt.scatter(x_2, y_2,color='blue')
    plt.scatter(x_3, y_3,color='black')
       
    plt.grid(True)
    plt.axis('scaled')
    plt.show()
# %%
def cobertura_v2(C,F,dmin,P,flag):
    x, y = P.T
    pgon = Polygon(zip(x, y))
    area_pol = pgon.area
    # k = max(math.floor(area_pol/(math.pi*dmin**2)),1); 
    k = 1

    raio_cobertura = sys.maxsize
    r = 0
    if( np.size(F,0) != 0):
        r = np.size(F,0)
    var_raio = []

    if (r > k) and (r != 0):
        k = r
    
    Pos = 0
    cluster = 0
    cluster_real = 0

    while raio_cobertura > dmin:

        raios = np.zeros(shape=(k,1))
        kmaean = kmean_cobertura(C,F,k,flag)
        it = kmaean["it"]
        Pos = kmaean["Pos"]
        D = kmaean["D"]
        cluster = kmaean["cluster"]
        cluster_real = kmaean["cluster_real"]

        for j in range(0,k):
            pos = Pos[j]
            raios[j] = np.amax(D[pos,j])
            

        raio_cobertura = np.mean(raios)
        # raio_cobertura = np.amax(raios)
        var_raio = np.concatenate((var_raio, (k, raio_cobertura))) 

        if raio_cobertura > dmin:
            k = k + max(math.floor(k*(raio_cobertura/dmin - 1)),1)

    return separa_cluster(C, cluster, P)

# %%

def dados():
    #F = np.empty(shape=(0, 2))
    flag = 1

    decision =  decision_variables()
    P = get_poligon()
    C = get_cluster()
    pos_real = C[:,0]
    ponto_fixo = np.array(list(map(bool,C[:,3])))
    C = C[:,1:3]
    dmin =float(decision['defaultBusStopRadius'])/(111.32/0.001)
    F = C[ponto_fixo,:]
    #C = np.delete(C,0,0)

    return{
        "C":C,
        "P":P,
        "F":F.reshape((1,2)),
        "flag":flag,
        "dmin":dmin,
        "pos_real": pos_real
    }

# %%
# Helper function to convert the coordinates into an adjacency matrix
def coordinates_to_adjacency_matrix(data):

    a = np.zeros((len(data),len(data)))
    for i in range(len(a)):
        for j in range(len(a)):
            if not i == j:
                a[i][j] = np.linalg.norm(data[i] - data[j])
    return a

# %%
class Chromosome():
    
    # Random generated Chromosome
    #  m - number of traveling salesmans
    def __init__(self, number_of_cities, number_of_traveling_salesman, adj):
        self.n = number_of_cities
        self.m = number_of_traveling_salesman
        self.adj = adj
        c = np.array(range(1,number_of_cities))
        np.random.shuffle(c)
        self.solution = np.array_split(c, self.m)
        for i in range(len(self.solution)):
            self.solution[i] = np.insert(self.solution[i],0,0)
            self.solution[i] = np.append(self.solution[i],0)
        self.fitness()
            
    # Evaluate the Chromosome - Fitness function
    #  based on 2 features: 
    #   - overall cost (cumulated from all salesman)
    #   - worst (longest) salesman cost
    #  adj - adjacency matrix
    def fitness(self):
        self.cost = 0
        longest_salesman_fitness = []
        longest_salesman_length = 0
        for i in range(self.m):
            salesman = self.solution[i]
            salesman_fitness = 0
            for j in range(len(salesman) - 1):
                salesman_fitness = salesman_fitness + self.adj[salesman[j]][salesman[j+1]]
            self.cost = self.cost + salesman_fitness
            if len(salesman) > longest_salesman_length or (len(salesman) == longest_salesman_length and salesman_fitness > self.minmax):
                longest_salesman_length = len(salesman)
                self.minmax = salesman_fitness
        self.score = self.cost + self.minmax
    
    # Mutation operator - mutates a single Traveling Salesman
    #  by swaping 2 cities
    def mutate_local(self):
        index = np.random.randint(0,self.m)
        mutant = self.solution[index]
        i,j = np.random.randint(1,len(mutant)-1), np.random.randint(1,len(mutant)-1)
        mutant[i], mutant[j] = mutant[j], mutant[i]
        old_cost = self.cost
        self.fitness()
    
    # Mutation operator - mutates 2 Traveling Salesmans
    #  by removing a city from a salesman and asigning it to the second one
    def mutate_global(self):
        for i in range(self.m):
            if len(self.solution[i]) < 3:
                print(i, self.solution[i])
        
        
        index1, index2 = np.random.randint(0,self.m), np.random.randint(0,self.m)
        while index1 == index2:
            index1, index2 = np.random.randint(0,self.m), np.random.randint(0,self.m)
        while len(self.solution[index1]) < 4:
            index1, index2 = np.random.randint(0,self.m), np.random.randint(0,self.m)
        mutant1, mutant2 = self.solution[index1], self.solution[index2]
        i,j = np.random.randint(1,len(mutant1)-1), np.random.randint(1,len(mutant2)-1)
        self.solution[index2] = np.insert(mutant2, j, mutant1[i])
        self.solution[index1] = np.delete(mutant1, i)
        old_cost = self.cost
        self.fitness()
    
    # PMX Crossover
    def crossover(self, chromosome):
        for index in range(self.m):
            salesman1, salesman2 = self.solution[index], chromosome.solution[index]
            for i in range(1,min(len(salesman1),len(salesman2))-1):
                if salesman2[i] in salesman1:
                    salesman1[i], salesman1[salesman1.tolist().index(salesman2[i])] = salesman1[salesman1.tolist().index(salesman2[i])], salesman1[i]
        self.fitness()
        

# %%
class Population():
    
    def __init__(self, population_size = 50, adj = [], number_of_cities = 0):
        self.population = []
        self.population_size = population_size
        self.adj = adj
        self.number_of_cities = number_of_cities
        for i in range(population_size):
            self.population.append(Chromosome(number_of_cities = number_of_cities, number_of_traveling_salesman = NUMBER_OF_TRAVELING_SALESMAN, adj = adj))
    
    # Genetic Algorithm
    def run_genetic_algorithm(self, number_of_iterations = 1000, mutation_probability = 0.7, crossover_probability = 0.7):
        
        # Run for a fixed number of iterations
        for it in range(number_of_iterations):
            
            # Tournament selection
            k = self.population_size
            j = (int)(self.population_size * 0.6)
            for _ in range(self.population_size - k):
                del self.population[-np.random.randint(0,len(self.population))]
            for _ in range(k - j):
                worst_chromosome_score = self.population[0].score
                worst_chromosome_index = 0
                for i in range(1,len(self.population)):
                    if self.population[i].score > worst_chromosome_score:
                        worst_chromosome_score = self.population[i].score
                        worst_chromosome_index = i
                del self.population[-worst_chromosome_index]
                
            for _ in range(self.population_size - len(self.population)):
                self.population.append(Chromosome(number_of_cities = self.number_of_cities, number_of_traveling_salesman = NUMBER_OF_TRAVELING_SALESMAN, adj = self.adj))
            
            # Mutate globally
            for index in range(len(self.population)):
                if np.random.random(1)[0] < mutation_probability:
                    chromosome = copy.deepcopy(self.population[index])
                    chromosome.mutate_global()
                    if chromosome.score < self.population[index].score:
                        self.population[index] = chromosome
                
            # Mutate locally
            for index in range(len(self.population)):
                if np.random.random(1)[0] < mutation_probability:
                    chromosome = copy.deepcopy(self.population[index])
                    chromosome.mutate_local()
                    if chromosome.score < self.population[index].score:
                        self.population[index] = chromosome
                
            # Crossover
            for index1 in range(len(self.population)):
                if np.random.random(1)[0] < crossover_probability:
                    index2 = np.random.randint(0,len(self.population))
                    if index1 == index2:
                        index2 = np.random.randint(0,len(self.population))
                    child1 = copy.deepcopy(self.population[index1])
                    child2 = copy.deepcopy(self.population[index2])
                    child1.crossover(self.population[index2])
                    child2.crossover(self.population[index1])
                    if child1.score < self.population[index1].score:
                        self.population[index1] = child1
                    if child2.score < self.population[index2].score:
                        self.population[index2] = child2
    
    # Print the overall cost and the minmax cost of the best chromosome 
    def get_best_result(self):
        best_chromosome = self.population[0]
        for i in range(1,self.population_size):
            if self.population[i].score < best_chromosome.score:
                best_chromosome = self.population[i]
       # print("Overall cost: ", best_chromosome.cost)
       # print("Minmax cost: ", best_chromosome.minmax)
    

# %% [markdown]
# ---
def PickupColor():
    color=random.choice(list(pltColor.colors.cnames.items()))[0]
    return color

def DrawNetwork(vlist,data):
    G = nx.DiGraph()
    locations = coordinates_to_adjacency_matrix(data)
    # print(locations)
    x = 0
    for vehicle_id in vlist:
        n = 0
        e = []
        node = []
        cl=PickupColor()

        for i in vehicle_id:
            G.add_node(i, pos=(locations[i][0], locations[i][1]))
            if n > 0:
                u = (vehicle_id[n - 1], vehicle_id[n])
                e.append(u)
                node.append(i)
                G.add_edge(vehicle_id[n - 1], vehicle_id[n])
                nx.draw(G, nx.get_node_attributes(G, 'pos'), nodelist=node, edgelist=e, with_labels=True,
                        node_color=cl, width=2, edge_color=cl,
                        style='dashed', font_color='w', font_size=12, font_family='sans-serif')
            n += 1
        x += 1
    # let's color the node 0 in black
    nx.draw_networkx_nodes(G, locations, nodelist=[0], node_color='k')
    plt.axis('on')
    plt.show()
# %% [markdown]
# ## Run the Genetic Algorithm

def roteamento(DM,data,NUMBER_OF_CITIES):  
    
    # pop = Population(population_size=POPULATION_SIZE, adj= coordinates_to_adjacency_matrix(data), number_of_cities= NUMBER_OF_CITIES)
    pop = Population(population_size=POPULATION_SIZE, adj= DM, number_of_cities= NUMBER_OF_CITIES)
    pop.run_genetic_algorithm(number_of_iterations=NUMBER_OF_ITERATIONS)
    pop.get_best_result()

    # %% [markdown]
    # ## Print best solution

    # %%
    # Iterate through population and get the best solution
    best_chromosome = pop.population[0]
    for i in range(1,pop.population_size):
        if pop.population[i].score < best_chromosome.score:
            best_chromosome = pop.population[i]
            
    # Print best solution
    vlist=[]
    for i in range(best_chromosome.m):
        route = []
        #print(i+1, ":  ", best_chromosome.solution[i][0]+1, end="", sep="")

        route.append(best_chromosome.solution[i][0])

        for j in range(1,len(best_chromosome.solution[i])):
            #print("-", best_chromosome.solution[i][j]+1, end="", sep="")
            route.append(best_chromosome.solution[i][j])
        #print(" --- #", len(best_chromosome.solution[i]))

        vlist.append(route)
    #print()

    # Print cost
    #print("Cost: ", best_chromosome.cost)
    #print("Minmax: ", best_chromosome.minmax)

    # DrawNetwork( vlist,data )

    return vlist
# %% [markdown]
# ---


# print(decision_variables())
# print(get_distance_data( 218, -6.090061, -42.817585, 961, -7.078291, -42.774065))
def ismember(A, B):
    return [ np.sum(a == B) for a in A ]

def  main():

    # dados iniciais
    init = dados()

    resultado_cobertura = cobertura_v2(init["C"],init["F"],init["dmin"],init["P"],init["flag"])
    cluster = resultado_cobertura['cluster']
    pos_cluster = resultado_cobertura['pos_cluster']
    latitude = init["C"][:,0][pos_cluster]
    longitude = init["C"][:,1][pos_cluster]

    DM = np.zeros((np.size(cluster,0),np.size(cluster,0)))
    TM = np.zeros((np.size(cluster,0),np.size(cluster,0)))

    index_matriz = []
    for i in range(np.size(cluster,0)):
        index_matriz.append(int(init["pos_real"][pos_cluster][i]))
        for j in range(np.size(cluster,0)):
            if i != j:
                idx_inicial = int(init["pos_real"][pos_cluster][i])
                idx_final = int(init["pos_real"][pos_cluster][j])
                lat_inicial = latitude[i]
                lat_final = latitude[j]
                long_inicial = longitude[i]
                long_final = longitude[j]

                resultado_get = get_distance_data(idx_inicial,lat_inicial,long_inicial,idx_final,lat_final,long_final)
                
                DM[i,j] = (resultado_get['distance'])
                TM[i,j] = (resultado_get['duration'])

    data = np.copy(cluster)
    NUMBER_OF_CITIES = len(data)
    rotas = roteamento(DM,data,NUMBER_OF_CITIES)

    rotas_finais = []
    for rota in rotas:
        rota_i = []
        for i in range(len(rota)):
            rota_i.append(index_matriz [rota[i]])
        rotas_finais.append(rota_i)

    # count_rota = 0
    # rotas_finais = []
    # pos_front_end = []
    # for rota in rotas:
    #     count_rota += 1
    #     aux_1 = np.in1d(init["C"],cluster[rota]) # ismember
    #     aux_2 = np.array(aux_1).reshape(init["C"].shape) # boolian matrix ismember
    #     aux_3 = np.sum(aux_2,1)
    #     for i in range(len(aux_3)):
    #         if aux_3[i] == 2:
    #             pos_front_end.append(i)     
        
    #     rotas_finais.append({"Rota %d" % (count_rota): init["pos_real"][pos_front_end]})

    salve_results(rotas_finais)
    return rotas_finais 

main()
    # cobertura_v2(init["C"],init["F"],init["dmin"],init["P"],init["flag"])
