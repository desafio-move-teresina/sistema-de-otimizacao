<?php

use App\Http\Controllers\api\ResourceConfiguration\BasicRulesController;
use App\Http\Controllers\api\ResourceCompany\CompaniesController;
use App\Http\Controllers\api\ResourceMap\MapEditorController;
use App\Http\Controllers\api\ResourceVehicles\VehiclesController;
use App\Http\Controllers\api\ResourceRoute\EngineController;
use App\Http\Controllers\api\ResourceRoute\RoutesController;
use App\Http\Controllers\api\ResourceRoute\OptimizationController;
use App\Http\Controllers\api\ResourceBusStop\BusStopController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::apiResource( 
        '/decision-variables', 
        BasicRulesController::class)->only(['index','store','show'])
                                    ->names('basicRule');
Route::apiResource( 
        '/map-editor', 
        MapEditorController::class)->only(['index','store','show'])
                                ->names('mapEditor');
Route::apiResource( 
        '/optimization', 
        OptimizationController::class)->only(['index'])
                                ->names('optimization');
Route::apiResource( 
        '/companies', 
        CompaniesController::class)->names('company');

Route::apiResource( 
        '/vehicles', 
        VehiclesController::class)->names('vehicles');

Route::apiResource( 
        '/engine', 
        EngineController::class)->names('engine');

Route::apiResource( 
        '/routes', 
        RoutesController::class)->names('routes');

Route::get('/reorderSingleRoute', [RoutesController::class, 'reorderSingleRoute']);

Route::post('/getBusStopsInsidePolygon', [BusStopController::class, 'getBusStopsInsidePolygon']);

Route::post('/getDistanceData', [BusStopController::class, 'getBusStopsDistanceData']);

Route::post('/map-process', [EngineController::class, 'processMapRutes']);
