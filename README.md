# Optime

Desenvolvimento **back-end** da aplicação de gerenciamento das empresas, ônibus, mapas, pontos de ônibus e rotas para a prefeitura de Teresina.

As tecnologias usadas são [PHP](https://www.php.net/), [Laravel](https://laravel.com/) e [MySQL](https://www.mysql.com/)

## Links

### Projeto

- [Entidades e Rotas](https://optime.atlassian.net/wiki/spaces/OPT/pages/289144834/Documenta+o+-+Entidades+e+Rotas) - documentação inicial da API
- [Bitbucket](https://bitbucket.org/desafio-move-teresina/painel-do-sistema-de-otimizacao/src/master) - repositório do projeto no Bitbucket
- [Jira](https://optime.atlassian.net/jira/software/projects/OPT/boards/1) - gerenciamento do projeto no Jira
- [Confluence](https://optime.atlassian.net/wiki/spaces/OPT/overview) - documentação do projeto no Confluence

### Docs e outros

- [Google Cloud Console](https://console.cloud.google.com/home/dashboard) - gerar chaves para API do Google Maps em _APIs e serviços_ > _Credenciais_
- [PHP](https://www.php.net/docs.php) - documentação php
- [Laravel](https://laravel.com/docs/8.x) - documentação do Laravel
- [Composer](https://getcomposer.org/doc/) - documentação para o gerenciador de dependências para PHP

## Variáveis de ambiente

Antes de executar o projeto é necessário configurar algumas variáveis de ambientes que não estão versionadas no Git. Para isso abra o arquivo `.env.example` e siga as instruções.

## Configurar e executar o projeto localmente


1 - Baixar as dependências do projeto com comando:
    
    composer install

2 - Criar o aquivo .env no projeto:

    cp .env.example .env
    
3 - Configurar o projeto no arquivo .env

    APP_NAME=Laravel
    APP_ENV=local
    APP_URL=http://localhost

    DB_CONNECTION= <mysql>
    DB_HOST= <host>
    DB_PORT= <port>
    DB_DATABASE= <data_base>
    DB_USERNAME= <user>
    DB_PASSWORD= <password>

    PRODATER_BASE_URL= <prodater_url>
    PRODATER_USER=  <prodater_user>
    PRODATER_PASSWORD= <prodater_password>

    ENGINE_URL= <optimization_service_url>

4 - Geração da Key, presente no arquivo .env com o comando:

    php artisan key:generate

5 - Executar a migration com o seguinte comando: 
    
    php artisan migrate

6 - Passo opcional, que é gerar informações "fakes", para abastercer as tabelas do banco de dados:

    php artisan db:seed

7 - Servidor HTTP na porta 8000:

    php artisan serve

## Versões

O projeto está sendo testado usando ferramentas nas seguintes versões:

```bash
php -v
PHP 7.4.3 

composer -v
version 2.1.14
```

## About Laravel 

<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400"></a></p>


Laravel is a web application framework with expressive, elegant syntax. We believe development must be an enjoyable and creative experience to be truly fulfilling. Laravel takes the pain out of development by easing common tasks used in many web projects, such as:

- [Simple, fast routing engine](https://laravel.com/docs/routing).
- [Powerful dependency injection container](https://laravel.com/docs/container).
- Multiple back-ends for [session](https://laravel.com/docs/session) and [cache](https://laravel.com/docs/cache) storage.
- Expressive, intuitive [database ORM](https://laravel.com/docs/eloquent).
- Database agnostic [schema migrations](https://laravel.com/docs/migrations).
- [Robust background job processing](https://laravel.com/docs/queues).
- [Real-time event broadcasting](https://laravel.com/docs/broadcasting).

Laravel is accessible, powerful, and provides tools required for large, robust applications.

## Learning Laravel

Laravel has the most extensive and thorough [documentation](https://laravel.com/docs) and video tutorial library of all modern web application frameworks, making it a breeze to get started with the framework.

If you don't feel like reading, [Laracasts](https://laracasts.com) can help. Laracasts contains over 1500 video tutorials on a range of topics including Laravel, modern PHP, unit testing, and JavaScript. Boost your skills by digging into our comprehensive video library.

## Laravel Sponsors

We would like to extend our thanks to the following sponsors for funding Laravel development. If you are interested in becoming a sponsor, please visit the Laravel [Patreon page](https://patreon.com/taylorotwell).

### Premium Partners

- **[Vehikl](https://vehikl.com/)**
- **[Tighten Co.](https://tighten.co)**
- **[Kirschbaum Development Group](https://kirschbaumdevelopment.com)**
- **[64 Robots](https://64robots.com)**
- **[Cubet Techno Labs](https://cubettech.com)**
- **[Cyber-Duck](https://cyber-duck.co.uk)**
- **[Many](https://www.many.co.uk)**
- **[Webdock, Fast VPS Hosting](https://www.webdock.io/en)**
- **[DevSquad](https://devsquad.com)**
- **[Curotec](https://www.curotec.com/)**
- **[OP.GG](https://op.gg)**

## Contributing

Thank you for considering contributing to the Laravel framework! The contribution guide can be found in the [Laravel documentation](https://laravel.com/docs/contributions).

## Code of Conduct

In order to ensure that the Laravel community is welcoming to all, please review and abide by the [Code of Conduct](https://laravel.com/docs/contributions#code-of-conduct).

## Security Vulnerabilities

If you discover a security vulnerability within Laravel, please send an e-mail to Taylor Otwell via [taylor@laravel.com](mailto:taylor@laravel.com). All security vulnerabilities will be promptly addressed.

## License

The Laravel framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).


This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact
